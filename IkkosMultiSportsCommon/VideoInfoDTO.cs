﻿using System;

namespace IkkosCopyMeSportsPortal
{
    public class VideoInfoDTO
    {
        public int Id { get; set; }
        public string VideoURL { get; set; }
        public string ActorName { get; set; }
        public string MomentName { get; set; }
        public string IntroVideoURL { get; set; }
        public int Status { get; set; }
        public int CreatedBy { get; set; }
        public int Duration { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ThumbNailURL { get; set; }
    }
}