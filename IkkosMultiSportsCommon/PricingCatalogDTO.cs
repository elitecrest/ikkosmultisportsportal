﻿using System;

namespace IkkosCopyMeSportsPortal
{
    public class PricingCatalogDTO
    {
        public int Id { get; set; }
        public string Price { get; set; }
        public string SKUID { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}