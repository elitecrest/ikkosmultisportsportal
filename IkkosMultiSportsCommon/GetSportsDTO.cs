﻿namespace IkkosCopyMeSportsPortal
{
    public class GetSportsDTO
    {
        public GetSportsDTO()
        {

        }

        public int SportId { get; set; }
        public string SportName { get; set; }
        public string SportsImageUrl { get; set; }
        public string SportsBannerImageUrl { get; set; }
    }
}