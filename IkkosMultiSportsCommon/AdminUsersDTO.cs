﻿using System;

namespace IkkosCopyMeSportsPortal
{
    public class AdminUsersDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public int SportId { get; set; }
        public string ChannelName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ProfilePicURL { get; set; }
    }
}