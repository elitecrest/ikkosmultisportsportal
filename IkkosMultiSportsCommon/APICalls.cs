﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace IkkosMultiSportsCommon
{
    public static class APICalls
    {
        static APICalls()
        {
            //initialize variables here
        }



        public static string baseurl = ConfigurationManager.AppSettings["BaseUrl"];
        public static CustomResponse res = new CustomResponse();
        public static System.Web.Script.Serialization.JavaScriptSerializer javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();



        public static CustomResponse Get(string apiurl)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseurl);
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(apiurl).Result;
            if (response.IsSuccessStatusCode)
            {
                res = response.Content.ReadAsAsync<CustomResponse>().Result;
            }
            return res;
        }

        public static CustomResponse Post(string apiurl, object data)
        {

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(baseurl);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.PostAsJsonAsync(apiurl, data).Result;
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsAsync<CustomResponse>().Result;
                }
                return res;
            }
            catch
            {
                throw;
            }
        }

        public static CustomResponse Put(string apiurl, object data)
        {
            javaScriptSerializer.MaxJsonLength = 2147483647;

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(baseurl);
                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string jsonstring = javaScriptSerializer.Serialize(data);
                StringContent content = new System.Net.Http.StringContent(jsonstring, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PutAsync(apiurl, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsAsync<CustomResponse>().Result;
                }
                else
                {
                }
                return res;

            }
            catch
            {
                throw;
            }

        }

        public static CustomResponse Delete(string apiurl)
        {

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(baseurl);
                HttpResponseMessage response = client.DeleteAsync(apiurl).Result;
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsAsync<CustomResponse>().Result;
                }
                return res;
            }
            catch
            {
                throw;
            }
        }

    }
}