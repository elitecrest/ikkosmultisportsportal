﻿using System;

namespace IkkosCopyMeSportsPortal
{
    public class PackagesInfoDTO
    {
        public int PackageId { get; set; }
        public string PackageName { get; set; }
        public string Description { get; set; }
        public int LibraryId { get; set; }
        public string Format { get; set; }
        public string VideoURL { get; set; }
        public string ThumbNailURL { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public int PricingCatalogId { get; set; }
        public string Price { get; set; }
        public string SkuId { get; set; }
        public int Duriation { get; set; }
        //public List<int> InstructionVideos { get; set; }
        //public List<int> TrainingVideos { get; set; }
    }
}