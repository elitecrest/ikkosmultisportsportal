﻿namespace IkkosMultiSportsCommon
{
    public class CustomResponse
    {
        public CustomResponseStatus Status;
        public object Response;
        public string Message;
    }
    public enum CustomResponseStatus
    {
        Successful,
        UnSuccessful,
        Exception
    }
}