﻿using System;
using FluentAssertions;
using IkkosMultiSportsCommon;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace IkkosMultiSportsTests
{
    [TestClass]
    public class MultiSportsTests
    {
        [TestMethod]
        public void UserLogInShouldNotReturnNull()
        {
            string UserName = "shaikabdulosman@gmail.com";
            string Password = "123";
            CustomResponse res = APICalls.Get("Users/UserLogin?emailaddress=" + UserName + "&password=" + Password);
            res.Should().NotBeNull();

        }
        [TestMethod]
        public void GetPricingCatalogTestShouldNotReturnNull()
        {
            CustomResponse result = APICalls.Get("Package/GetPricingCatalog");
            result.Should().NotBeNull();

        }
    }


}
