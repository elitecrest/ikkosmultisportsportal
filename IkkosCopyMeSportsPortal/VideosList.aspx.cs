﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using Microsoft.WindowsAzure.MediaServices.Client;
using System.Web.Script.Serialization;
using IkkosMultiSportsCommon;

namespace IkkosCopyMeSportsPortal
{
    public partial class VideosList : System.Web.UI.Page
    {
        //public static SqlConnection ConnectTodb()
        //{

        //    string connectionString = ConfigurationManager.ConnectionStrings["LemonaidDB"].ConnectionString;
        //    SqlConnection sqlconn = new SqlConnection(connectionString);
        //    sqlconn.Open();
        //    return sqlconn;
        //}
        CustomResponse Result = new CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null)
            {
                Response.Redirect("Login.aspx", false);
            }
            //SqlConnection myConn = ConnectTodb();
            //StringBuilder query = new StringBuilder();
            //query.Append(@"Select VideoURL from videos where id in(112,113,114,115,116)");
            //using (SqlCommand cmd = new SqlCommand(query.ToString()))
            ////using (SqlCommand cmd = new SqlCommand("select * from AtheleteInstituteMatches  where  Convert(varchar,CreatedDate,101) BETWEEN '02/01/2016'  AND '02/14/2016' and CollAtheleteCoachType = 1"))
            //{
            //    using (SqlDataAdapter sda = new SqlDataAdapter())
            //    {
            //        cmd.Connection = myConn;
            //        sda.SelectCommand = cmd;
            //        using (DataTable dt = new DataTable())
            //        {
            //            sda.Fill(dt);
            //            dtVideos.DataSource = dt;
            //            dtVideos.DataBind();
            //            dtVideos.RepeatColumns = 4;
            //            dtVideos.RepeatDirection = RepeatDirection.Horizontal;
            //        }
            //    }
            //}
            //myConn.Close();
            int LogUserId = (int)System.Web.HttpContext.Current.Session["UserId"];
            CustomResponse Result1 = APICalls.Get("Videos/GetAdminVideos?adminUserId=" + LogUserId);
            if (Result1.Response != null)
            {
                JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                serializer2.MaxJsonLength = 1000000000;
                var Videos = Result1.Response.ToString();

                List<VideoInfoDTO> VideosList = serializer2.Deserialize<List<VideoInfoDTO>>(Videos);
                //var VideosJson = new JavaScriptSerializer().Serialize(VideosList);
                //var replVideosJson =  VideosJson.Replace("\u0026", "&");

                //DataTable UserVideosTable = JsonStringToDataTable(replVideosJson);
                ////SportsTable.Rows.Add("Select Sport", "Select Sport");
                //DataView view = UserVideosTable.DefaultView;
                dtVideos.DataSource = VideosList;
                dtVideos.RepeatColumns = 4;
                dtVideos.RepeatDirection = RepeatDirection.Horizontal;
                dtVideos.DataBind();
            }
            else
            {

            }
        }

        protected void UploadVideos_Click(object sender, EventArgs e)
        {
            if (BulkUpload.HasFile)
            {
                //    HttpFileCollection file = Request.Files;
                //    int TotalCount = file.Count;
                //    for (int i = 0; i <= file.Count - 1; i++)
                //    {
                //        HttpPostedFile hpf = file[i];
                //        var type = hpf.ContentType.ToString();
                //        var VideoName = hpf.FileName;
                //        if (hpf.ContentLength > 0)
                //        {


                //        }
                //    }
                //}
                //else
                //{
                //    lblFileList.Text = "Please Select Atleast One Video..";

                //}
                VideoInfoDTO video = new VideoInfoDTO();
                HttpFileCollection files = Request.Files;
                int TotalCount = files.Count;
                for (int i = 0; i < files.Count; i++)
                {

                    HttpPostedFile hpf = files[i];
                    string fileName = "";
                    string videoURL = "";
                    using (MemoryStream ms = new MemoryStream())
                    {
                        fileName = hpf.FileName;
                        ms.SetLength(hpf.ContentLength);
                        hpf.InputStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        string base64String = Convert.ToBase64String(ms.ToArray());
                        string format = hpf.ContentType.Split('/')[1];
                        videoURL = streamfile(ms, format);
                    }
                    int UserId = (int)System.Web.HttpContext.Current.Session["UserId"];
                    video.VideoURL = videoURL;
                    string thumbnail = GetVideoThumbnails(video);
                    video.ThumbNailURL = thumbnail;
                    video.CreatedBy = UserId;
                    video.MomentName = fileName;
                    video.ActorName = "Athlete";
                    CustomResponse res = APICalls.Post("Videos/AddOrUpdateAdminVideo", video);
                    int VideoID = Convert.ToInt32(res.Response.ToString());
                    if (VideoID != 0)
                    {
                        lblFileList.Text = res.Message;
                        lblFileList.ForeColor = System.Drawing.Color.Green;
                        GetVideosList();
                    }
                    else
                    {
                        lblFileList.Text = res.Message;
                        lblFileList.ForeColor = System.Drawing.Color.Red;

                    }
                }
                // VideoInfoDTO video = new VideoInfoDTO();
                // HttpPostedFile file1 = Request.Files["BulkUpload"];    
            }
            else
            {

                lblFileList.Text = "Please Select Atleast One file to Upload!!";
                lblFileList.ForeColor = System.Drawing.Color.Red;
            }
            GetVideosList();
        }

        private void GetVideosList()
        {
            int LogUserId = (int)System.Web.HttpContext.Current.Session["UserId"];
            CustomResponse Result1 = APICalls.Get("Videos/GetAdminVideos?adminUserId=" + LogUserId);
            if (Result1.Response != null)
            {
                JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                serializer2.MaxJsonLength = 1000000000;
                var Videos = Result1.Response.ToString();

                List<VideoInfoDTO> VideosList = serializer2.Deserialize<List<VideoInfoDTO>>(Videos);
                //var VideosJson = new JavaScriptSerializer().Serialize(VideosList);
                //var replVideosJson =  VideosJson.Replace("\u0026", "&");

                //DataTable UserVideosTable = JsonStringToDataTable(replVideosJson);
                ////SportsTable.Rows.Add("Select Sport", "Select Sport");
                //DataView view = UserVideosTable.DefaultView;
                dtVideos.DataSource = VideosList;
                dtVideos.RepeatColumns = 4;
                dtVideos.RepeatDirection = RepeatDirection.Horizontal;
                dtVideos.DataBind();
            }
            else
            {

            }
        }
        private string GetVideoThumbnails(VideoInfoDTO videoResp)
        {
            VideoInfoDTO video = new VideoInfoDTO();
            UploadImage uplImage = new UploadImage();
            ////Delete all Video thumbnails 
            //UsersDalV5.DeleteAthleteVideoThumbnails(videoResp.Id);

            var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
            int totDuration = videoResp.Duration;
            float startPoint = 0;
            float dur = (float)totDuration / 6;

            var frame = startPoint + (float)Math.Round(2.0, 2);
            string thumbnailJpeGpath = HttpContext.Current.Server.MapPath("~/videoThumb" + 1 + ".jpg");
            ffMpeg.GetVideoThumbnail(videoResp.VideoURL, thumbnailJpeGpath, frame);
            byte[] bytes = File.ReadAllBytes(thumbnailJpeGpath);
            string base64String = Convert.ToBase64String(bytes);
            string thumbnailUrl = uplImage.GetImageURL(base64String, ".jpg", "");

            //Add Image to the DB 

            startPoint = startPoint + (float)Math.Round(dur, 2);
            return thumbnailUrl;


        }


        private string streamfile(MemoryStream stream, string format)
        {

            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);

            var fileStream = System.IO.File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();

            var ext = format;

            Stream fileStreambanners = stream;


            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string fileURL = UploadFile(fileNamebanners, path, ext, fileStreambanners);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);

            }
            //finalVideoUrl = videoURL;
            return fileURL;
        }

        private string UploadFile(string fileName, string path, string ext, Stream InputStream)
        {

            string account = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountName"].ToString();
            string key = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountKey"].ToString();


            CloudMediaContext context = new CloudMediaContext(account, key);

            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);

            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));

            assetFile.Upload(path);


            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 365;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive),
                                                     AccessPermissions.Read);
            string streamingUrl = string.Empty;
            var assetFiles = streamingAsset.AssetFiles.ToList();
            var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith("." + ext)).FirstOrDefault();
            if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            {
                var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                var mp4Uri = new UriBuilder(locator.Path);
                mp4Uri.Path += "/" + streamingAssetFile.Name;
                streamingUrl = mp4Uri.ToString();
            }
            return streamingUrl;

        }
    }
}