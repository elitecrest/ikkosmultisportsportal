﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using Microsoft.WindowsAzure.MediaServices.Client;
using IkkosMultiSportsCommon;

namespace IkkosCopyMeSportsPortal
{
    public partial class PackagesCreation : System.Web.UI.Page
    {
        //public static SqlConnection ConnectTodb()
        //{
        //    string connectionString = ConfigurationManager.ConnectionStrings["LemonaidDB"].ConnectionString;
        //    SqlConnection sqlconn = new SqlConnection(connectionString);
        //    sqlconn.Open();
        //    return sqlconn;
        //}

        CustomResponse Result = new CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null)
            {
                Response.Redirect("Login.aspx", false);
            }
            Session.Timeout = 60;
            CustomResponse result = APICalls.Get("Package/GetPricingCatalog");
            if (result.Response != null)
            {
                JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                serializer2.MaxJsonLength = 1000000000;
                var sports = result.Response.ToString();

                List<PricingCatalogDTO> PricingData = serializer2.Deserialize<List<PricingCatalogDTO>>(sports);
                var PricingJson = new JavaScriptSerializer().Serialize(PricingData);

                DataTable PackagePricingTable = JsonStringToDataTable(PricingJson);
                //SportsTable.Rows.Add("Select Sport", "Select Sport");
                //DataView view = PackagePricingTable.DefaultView;
                //view.Sort = "Id";
                //ddlPricing.DataSource = view;
                //ddlPricing.DataValueField = "Id";
                //ddlPricing.DataTextField = "Price";
                //ddlPricing.DataBind();
                DataRow[] dr = PackagePricingTable.Select("PricingType=2");
                DataTable newTable = new DataTable();
                newTable = PackagePricingTable.Clone();
                foreach (DataRow row in dr)
                {
                    newTable.ImportRow(row);
                }

                DataView view = newTable.DefaultView;
                view.Sort = "Id";
                ddlPricing.DataSource = view;
                ddlPricing.DataValueField = "Id";
                ddlPricing.DataTextField = "Price";
                ddlPricing.DataBind();
            }
            else
            {

            }
            //SqlConnection myConn = ConnectTodb();
            //StringBuilder query = new StringBuilder();
            //query.Append(@"Select VideoURL from videos where id in(112,113,114,115,116)");
            //using (SqlCommand cmd = new SqlCommand(query.ToString()))
            ////using (SqlCommand cmd = new SqlCommand("select * from AtheleteInstituteMatches  where  Convert(varchar,CreatedDate,101) BETWEEN '02/01/2016'  AND '02/14/2016' and CollAtheleteCoachType = 1"))
            //{
            //    using (SqlDataAdapter sda = new SqlDataAdapter())
            //    {
            //        cmd.Connection = myConn;
            //        sda.SelectCommand = cmd;
            //        using (DataTable dt = new DataTable())
            //        {
            //            sda.Fill(dt);
            //            dtVideos.DataSource = dt;
            //            dtVideos.RepeatColumns = 2;
            //            dtVideos.RepeatDirection = RepeatDirection.Horizontal;
            //            dtVideos.DataBind();
            //        }
            //    }
            //}
            //myConn.Close();
            int LogUserId = (int)System.Web.HttpContext.Current.Session["UserId"];
            CustomResponse Result1 = APICalls.Get("Videos/GetAdminVideos?adminUserId=" + LogUserId);
            if (Result1.Response != null)
            {
                JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                serializer2.MaxJsonLength = 1000000000;
                var Videos = Result1.Response.ToString();

                List<VideoInfoDTO> VideosList = serializer2.Deserialize<List<VideoInfoDTO>>(Videos);
                //var VideosJson = new JavaScriptSerializer().Serialize(VideosList);
                //var replVideosJson =  VideosJson.Replace("\u0026", "&");

                //DataTable UserVideosTable = JsonStringToDataTable(replVideosJson);
                ////SportsTable.Rows.Add("Select Sport", "Select Sport");
                //DataView view = UserVideosTable.DefaultView;
                dtVideos.DataSource = VideosList;
                dtVideos.RepeatColumns = 2;
                dtVideos.RepeatDirection = RepeatDirection.Horizontal;
                dtVideos.DataBind();
            }
            else
            {

            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {

            if (ddlPricing.SelectedIndex != 0 && txtPackageName.Text != null && up1.HasFile)
            {
                string PackageName = txtPackageName.Text;
                string PackageDesc = txtDescription.Text;

                PackagesInfoDTO PackageDetails = new PackagesInfoDTO();
                HttpPostedFile file1 = Request.Files["up1"];
                string fileName = "";
                string videoURL = "";
                using (MemoryStream ms = new MemoryStream())
                {
                    fileName = file1.FileName;
                    ms.SetLength(file1.ContentLength);
                    file1.InputStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    string base64String = Convert.ToBase64String(ms.ToArray());
                    string format = file1.ContentType.Split('/')[1];
                    videoURL = streamfile(ms, format);
                }
                PackageDetails.PackageId = 0;
                PackageDetails.PackageName = PackageName;
                PackageDetails.Description = PackageDesc;
                PackageDetails.VideoURL = videoURL;
                string thumbnail = GetVideoThumbnails(PackageDetails);

                int UserId = (int)System.Web.HttpContext.Current.Session["UserId"];

                PackageDetails.ThumbNailURL = thumbnail;
                PackageDetails.CreatedDate = DateTime.Now;
                PackageDetails.CreatedBy = UserId;
                PackageDetails.PricingCatalogId = Convert.ToInt32(ddlPricing.SelectedItem.Value);

                if (lbldvDest1.Value != string.Empty)
                {
                    List<PackageVideoInfo> VideosList = new List<PackageVideoInfo>();
                    VideosList = GetVideoList();

                    PackageDetails.PackageVideoDetails = VideosList;

                    CustomResponse res = APICalls.Post("Package/InsertOrUpdatePackage", PackageDetails);
                    int PackageId = Convert.ToInt32(res.Response.ToString());
                    if (PackageId != 0)
                    {
                        Error.Text = res.Message;
                        Error.ForeColor = System.Drawing.Color.Green;
                        txtPackageName.Text = "";
                        txtDescription.Text = "";
                        ddlPricing.SelectedIndex = 0;
                    }
                    else
                    {
                        Error.Text = res.Message;
                        Error.ForeColor = System.Drawing.Color.Red;
                    }
                }
                else
                {
                    lblError.Text = "Please Select Atleast One training Video To Create package!!";
                    lblError.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                Error.Text = "Please Select All Mandatory Fields!!!";

            }
        }

        private List<PackageVideoInfo> GetVideoList()
        {
            List<PackageVideoInfo> VideosList = new List<PackageVideoInfo>();


            if (lbldvDest1.Value != string.Empty)
            {
                PackageVideoInfo VideoInfo = new PackageVideoInfo();

                VideoInfo.PreIntroVideoId = string.IsNullOrWhiteSpace(lbldvDest0.Value) ? 0 : Convert.ToInt32(lbldvDest0.Value);
                VideoInfo.ActualVideoId = Convert.ToInt16(lbldvDest1.Value);
                VideoInfo.PostIntroVideoId = string.IsNullOrWhiteSpace(lbldvDest2.Value) ? 0 : Convert.ToInt32(lbldvDest2.Value);
                VideosList.Add(VideoInfo);

            }
            else { return VideosList; }
            if (lbldvDest4.Value != string.Empty)
            {
                PackageVideoInfo VideoInfo = new PackageVideoInfo();
                VideoInfo.PreIntroVideoId = string.IsNullOrWhiteSpace(lbldvDest3.Value) ? 0 : Convert.ToInt32(lbldvDest3.Value);
                VideoInfo.ActualVideoId = Convert.ToInt16(lbldvDest4.Value);
                VideoInfo.PostIntroVideoId = string.IsNullOrWhiteSpace(lbldvDest5.Value) ? 0 : Convert.ToInt32(lbldvDest5.Value);
                VideosList.Add(VideoInfo);
            }
            else
            { return VideosList; }
            if (lbldvDest7.Value != string.Empty)
            {
                PackageVideoInfo VideoInfo = new PackageVideoInfo();
                VideoInfo.PreIntroVideoId = string.IsNullOrWhiteSpace(lbldvDest6.Value) ? 0 : Convert.ToInt32(lbldvDest6.Value);
                VideoInfo.ActualVideoId = Convert.ToInt16(lbldvDest7.Value);
                VideoInfo.PostIntroVideoId = string.IsNullOrWhiteSpace(lbldvDest8.Value) ? 0 : Convert.ToInt32(lbldvDest8.Value);
                VideosList.Add(VideoInfo);
            }
            else
            { return VideosList; }
            if (lbldvDest10.Value != string.Empty)
            {
                PackageVideoInfo VideoInfo = new PackageVideoInfo();
                VideoInfo.PreIntroVideoId = string.IsNullOrWhiteSpace(lbldvDest9.Value) ? 0 : Convert.ToInt32(lbldvDest9.Value);
                VideoInfo.ActualVideoId = Convert.ToInt16(lbldvDest10.Value);
                VideoInfo.PostIntroVideoId = string.IsNullOrWhiteSpace(lbldvDest11.Value) ? 0 : Convert.ToInt32(lbldvDest11.Value);
                VideosList.Add(VideoInfo);
            }
            else
            { return VideosList; }
            if (lbldvDest13.Value != string.Empty)
            {
                PackageVideoInfo VideoInfo = new PackageVideoInfo();
                VideoInfo.PreIntroVideoId = string.IsNullOrWhiteSpace(lbldvDest12.Value) ? 0 : Convert.ToInt32(lbldvDest12.Value);
                VideoInfo.ActualVideoId = Convert.ToInt16(lbldvDest13.Value);
                VideoInfo.PostIntroVideoId = string.IsNullOrWhiteSpace(lbldvDest14.Value) ? 0 : Convert.ToInt32(lbldvDest14.Value);
                VideosList.Add(VideoInfo);
            }
            else
            { return VideosList; }

            return VideosList;
        }
        public DataTable JsonStringToDataTable(string jsonString)
        {
            DataTable dt = new DataTable();
            string[] jsonStringArray = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{");
            List<string> ColumnsName = new List<string>();
            foreach (string jSA in jsonStringArray)
            {
                string[] jsonStringData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                foreach (string ColumnsNameData in jsonStringData)
                {
                    try
                    {
                        int idx = ColumnsNameData.IndexOf(":");
                        string ColumnsNameString = ColumnsNameData.Substring(0, idx - 1).Replace("\"", "");
                        if (!ColumnsName.Contains(ColumnsNameString))
                        {
                            ColumnsName.Add(ColumnsNameString);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Error Parsing Column Name : {0}", ColumnsNameData));
                    }
                }
                break;
            }
            foreach (string AddColumnName in ColumnsName)
            {
                dt.Columns.Add(AddColumnName);
            }
            foreach (string jSA in jsonStringArray)
            {
                string[] RowData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                DataRow nr = dt.NewRow();
                foreach (string rowData in RowData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        string RowColumns = rowData.Substring(0, idx - 1).Replace("\"", "");
                        string RowDataString = rowData.Substring(idx + 1).Replace("\"", "");
                        nr[RowColumns] = RowDataString;
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
                dt.Rows.Add(nr);
            }
            return dt;
        }

        private string GetVideoThumbnails(PackagesInfoDTO PackResp)
        {
            //  PackagesInfoDTO Package = new PackagesInfoDTO();
            UploadImage uplImage = new UploadImage();
            ////Delete all Video thumbnails 
            //UsersDalV5.DeleteAthleteVideoThumbnails(videoResp.Id);

            var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
            int totDuration = 0;// PackResp.Duriation;
            float startPoint = 0;
            float dur = (float)totDuration / 6;

            var frame = startPoint + (float)Math.Round(2.0, 2);
            string thumbnailJpeGpath = HttpContext.Current.Server.MapPath("~/videoThumb" + 1 + ".jpg");
            ffMpeg.GetVideoThumbnail(PackResp.VideoURL, thumbnailJpeGpath, frame);
            byte[] bytes = File.ReadAllBytes(thumbnailJpeGpath);
            string base64String = Convert.ToBase64String(bytes);
            string thumbnailUrl = uplImage.GetImageURL(base64String, ".jpg", "");

            //Add Image to the DB 

            startPoint = startPoint + (float)Math.Round(dur, 2);
            return thumbnailUrl;


        }

        private string streamfile(MemoryStream stream, string format)
        {

            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);

            var fileStream = System.IO.File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();

            var ext = format;

            Stream fileStreambanners = stream;


            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string fileURL = UploadFile(fileNamebanners, path, ext, fileStreambanners);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);

            }
            //finalVideoUrl = videoURL;
            return fileURL;
        }

        private string UploadFile(string fileName, string path, string ext, Stream InputStream)
        {

            string account = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountName"].ToString();
            string key = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountKey"].ToString();


            CloudMediaContext context = new CloudMediaContext(account, key);

            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);

            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));

            assetFile.Upload(path);


            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 365;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive),
                                                     AccessPermissions.Read);
            string streamingUrl = string.Empty;
            var assetFiles = streamingAsset.AssetFiles.ToList();
            var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith("." + ext)).FirstOrDefault();
            if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            {
                var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                var mp4Uri = new UriBuilder(locator.Path);
                mp4Uri.Path += "/" + streamingAssetFile.Name;
                streamingUrl = mp4Uri.ToString();
            }
            return streamingUrl;

        }


    }
}