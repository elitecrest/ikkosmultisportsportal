﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using IkkosMultiSportsCommon;

namespace IkkosCopyMeSportsPortal
{
    public partial class PackagesList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx", false);
                }
                int LogUserId = (int)System.Web.HttpContext.Current.Session["UserId"];
                CustomResponse Result1 = APICalls.Get("Package/GetPackagesByChannelId?channelId=" + LogUserId);
                if (Result1.Response != null)
                {
                    JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                    serializer2.MaxJsonLength = 1000000000;
                    var Packages = Result1.Response.ToString();

                    List<PackagesInfoDTO> PackagesList = serializer2.Deserialize<List<PackagesInfoDTO>>(Packages);
                    //var VideosJson = new JavaScriptSerializer().Serialize(VideosList);
                    //var replVideosJson =  VideosJson.Replace("\u0026", "&");

                    //DataTable UserVideosTable = JsonStringToDataTable(replVideosJson);
                    ////SportsTable.Rows.Add("Select Sport", "Select Sport");
                    //DataView view = UserVideosTable.DefaultView;
                    dtPackages.DataSource = PackagesList;
                    dtPackages.RepeatColumns = 3;
                    dtPackages.RepeatDirection = RepeatDirection.Horizontal;
                    dtPackages.DataBind();
                }
                else
                {
                    lblNoData.Text = "No Packages To Display!!!";
                    lblNoData.ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void dtPackages_ItemCommand(object source, DataListCommandEventArgs e)
        {
            switch (e.CommandName)
            {

                case "cmd_Edit":
                    dtPackages.EditItemIndex = e.Item.ItemIndex;
                    ListCustomers();
                    DataView PricingView = PackagePricing();
                    DropDownList ddledit = (DropDownList)dtPackages.Items[dtPackages.EditItemIndex].FindControl("ddl");
                    Label lblPrice = (Label)dtPackages.Items[dtPackages.EditItemIndex].FindControl("lblCatlog");

                    ddledit.DataSource = PricingView;
                    ddledit.DataValueField = "Id";
                    ddledit.DataTextField = "Price";
                    ddledit.DataBind();

                    ddledit.SelectedIndex = Convert.ToInt32(lblPrice.Text);

                    break;



                case "cmd_Update":

                    int packageId =Convert.ToInt32(((Label)e.Item.FindControl("lblEditPackageId")).Text);
                    string Packagename =  ((TextBox)e.Item.FindControl("txtPName")).Text;  
                    string Description =  ((TextBox)e.Item.FindControl("txtDe")).Text;  
                    int UpdatedPricingCatlog =Convert.ToInt32(((DropDownList)e.Item.FindControl("ddl")).SelectedItem.Value);


                    PackagesInfoDTO UpdatedPackageInfo = new PackagesInfoDTO();
                    UpdatedPackageInfo.PackageId = packageId;
                    UpdatedPackageInfo.PackageName = Packagename;
                    UpdatedPackageInfo.Description = Description;
                    UpdatedPackageInfo.PricingCatalogId = UpdatedPricingCatlog;
                    CustomResponse res = APICalls.Post("Package/InsertOrUpdatePackage", UpdatedPackageInfo);

                       int PackageId = Convert.ToInt32(res.Response.ToString());
                    if (PackageId != 0)
                    {
                        dtPackages.EditItemIndex = -1;
                        ListCustomers();
                    }
                    else
                    {
                      
                    }
                    break;


                case "cmd_Cancel":
                    dtPackages.EditItemIndex = -1;
                    ListCustomers();
                    break;

            }
        }
        private void ListCustomers()
        {
            int LogUserId = (int)System.Web.HttpContext.Current.Session["UserId"];
            CustomResponse Result1 = APICalls.Get("Package/GetPackagesByChannelId?channelId=" + LogUserId);
            if (Result1.Response != null)
            {
                JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                serializer2.MaxJsonLength = 1000000000;
                var Packages = Result1.Response.ToString();

                List<PackagesInfoDTO> PackagesList = serializer2.Deserialize<List<PackagesInfoDTO>>(Packages);
                //var VideosJson = new JavaScriptSerializer().Serialize(VideosList);
                //var replVideosJson =  VideosJson.Replace("\u0026", "&");

                //DataTable UserVideosTable = JsonStringToDataTable(replVideosJson);
                ////SportsTable.Rows.Add("Select Sport", "Select Sport");
                //DataView view = UserVideosTable.DefaultView;
                dtPackages.DataSource = PackagesList;
                dtPackages.RepeatColumns = 3;
                dtPackages.RepeatDirection = RepeatDirection.Horizontal;
                dtPackages.DataBind();
            }
            else
            {
                lblNoData.Text = "No Packages To Display!!!";
                lblNoData.ForeColor = System.Drawing.Color.Red;
            }
        }
        private DataView PackagePricing()
        {

            CustomResponse result = APICalls.Get("Package/GetPricingCatalog");
            JavaScriptSerializer serializer2 = new JavaScriptSerializer();
            serializer2.MaxJsonLength = 1000000000;
            var sports = result.Response.ToString();

            List<PricingCatalogDTO> PricingData = serializer2.Deserialize<List<PricingCatalogDTO>>(sports);
            var PricingJson = new JavaScriptSerializer().Serialize(PricingData);

            DataTable PackagePricingTable = JsonStringToDataTable(PricingJson);
            //SportsTable.Rows.Add("Select Sport", "Select Sport");
            //DataView view = PackagePricingTable.DefaultView;
            //view.Sort = "Id";
            //ddlPricing.DataSource = view;
            //ddlPricing.DataValueField = "Id";
            //ddlPricing.DataTextField = "Price";
            //ddlPricing.DataBind();
            DataRow[] dr = PackagePricingTable.Select("PricingType=2");
            DataTable newTable = new DataTable();
            newTable = PackagePricingTable.Clone();
            foreach (DataRow row in dr)
            {
                newTable.ImportRow(row);
            }

            DataView view = newTable.DefaultView;
            view.Sort = "Id";
            return view;
            //ddl.DataSource = view;
            //ddlPricing.DataValueField = "Id";
            //ddlPricing.DataTextField = "Price";
            //ddlPricing.DataBind();


        }
        public DataTable JsonStringToDataTable(string jsonString)
        {
            DataTable dt = new DataTable();
            string[] jsonStringArray = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{");
            List<string> ColumnsName = new List<string>();
            foreach (string jSA in jsonStringArray)
            {
                string[] jsonStringData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                foreach (string ColumnsNameData in jsonStringData)
                {
                    try
                    {
                        int idx = ColumnsNameData.IndexOf(":");
                        string ColumnsNameString = ColumnsNameData.Substring(0, idx - 1).Replace("\"", "");
                        if (!ColumnsName.Contains(ColumnsNameString))
                        {
                            ColumnsName.Add(ColumnsNameString);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Error Parsing Column Name : {0}", ColumnsNameData));
                    }
                }
                break;
            }
            foreach (string AddColumnName in ColumnsName)
            {
                dt.Columns.Add(AddColumnName);
            }
            foreach (string jSA in jsonStringArray)
            {
                string[] RowData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                DataRow nr = dt.NewRow();
                foreach (string rowData in RowData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        string RowColumns = rowData.Substring(0, idx - 1).Replace("\"", "");
                        string RowDataString = rowData.Substring(idx + 1).Replace("\"", "");
                        nr[RowColumns] = RowDataString;
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
                dt.Rows.Add(nr);
            }
            return dt;
        }
    }
}