﻿using System;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;

namespace IkkosCopyMeSportsPortal
{
    public class UploadImage
    {
        public string GetImageURL(string binary, string format, string contentType)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();
                string filename = "" + r.Next(999999) + DateTime.Now.Millisecond + r.Next(99999) + format;
                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();
                container.SetPermissions(new BlobContainerPermissions
                {
                    PublicAccess =
                        BlobContainerPublicAccessType.Blob
                });
                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
                // Create or overwrite the "myblob" blob with contents from a local file.
                byte[] binarydata = Convert.FromBase64String(binary);
                blockBlob.Properties.ContentType = contentType;
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);
                //UploadFromStream(fileStream);
                //}
                // Retrieve reference to a blob named "myblob".
                CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);
                imageurl = blockBlob1.Uri.ToString();
            }
            catch (Exception ex)
            {

            }
            return imageurl;
        }
    }
}