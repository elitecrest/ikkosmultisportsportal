﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using IkkosMultiSportsCommon;

namespace IkkosCopyMeSportsPortal
{
    public partial class UploadVideo : System.Web.UI.Page
    {
        public static SqlConnection ConnectTodb()
        {

            string connectionString = ConfigurationManager.ConnectionStrings["LemonaidDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }
        CustomResponse Result = new CustomResponse();

        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection myConn = ConnectTodb();
            StringBuilder query = new StringBuilder();
            query.Append(@"Select VideoURL from videos where id in(112,113,114,115,116)");
            using (SqlCommand cmd = new SqlCommand(query.ToString()))
            //using (SqlCommand cmd = new SqlCommand("select * from AtheleteInstituteMatches  where  Convert(varchar,CreatedDate,101) BETWEEN '02/01/2016'  AND '02/14/2016' and CollAtheleteCoachType = 1"))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = myConn;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        gd.DataSource = dt;
                        gd.DataBind();
                    }
                }
            }
            myConn.Close();
        }
        protected void UploadVideos_Click(object sender, EventArgs e)
        {
            //if (BulkUpload.HasFile)
            //{
            //    HttpFileCollection file = Request.Files;
            //    int TotalCount = file.Count;
            //    for (int i = 0; i <= file.Count - 1; i++)
            //    {
            //        HttpPostedFile hpf = file[i];
            //        var type = hpf.ContentType.ToString();
            //        var VideoName = hpf.FileName;
            //        if (hpf.ContentLength > 0)
            //        {
            //            Result = Handlefile(VideoName);
            //            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //            string jsonString = javaScriptSerializer.Serialize(Result);
            //            Response.Write(jsonString);

            //        }
            //    }
            //}
            //else
            //{
            //    lblFileList.Text = "Please Select Atleast One Video..";

            //}
        }
        //private CustomResponse Handlefile(string VideoName)
        //{
        //    string baseURL = ConfigurationManager.AppSettings["BaseURl"].ToString();
        //    int length = (int)Request.InputStream.Length;
        //    byte[] buffer = new byte[length];
        //    Stream stream = Request.InputStream;
        //    string VideoFormat = Request.QueryString["VideoFormat"].Trim();


        //    VideoDTO video = new VideoDTO();
        //    string videoURL = streamaudio(stream, VideoFormat);

        //    HttpClient client = new HttpClient();
        //    client.BaseAddress = new Uri(baseURL);
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


        //    //video.Id = 0;
        //    video.VideoName = VideoName;
        //    video.VideoURL = videoURL;
        //    video.Status = 1;

        //    var response = client.PostAsJsonAsync("Athlete/AddVideos", video).Result;

        //    if (response.IsSuccessStatusCode)
        //    {
        //        CustomResponse res = response.Content.ReadAsAsync<CustomResponse>().Result;

        //        Result.Status = CustomResponseStatus.Successful;
        //        Result.Response = res.Response;
        //        Result.Message = "Video_Added";

        //    }
        //    else
        //    {
        //        Result.Status = CustomResponseStatus.UnSuccessful;

        //        Result.Message = "NoRecordsFound";
        //    }
        //    return Result;
        //}


        //public string streamaudio(Stream stream, string format)
        //{

        //    Random ran = new Random();
        //    var fileNamebanners = "fn" + ran.Next() + "." + format;
        //    var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);

        //    var fileStream = System.IO.File.Create(path);
        //    stream.Seek(0, SeekOrigin.Begin);
        //    stream.CopyTo(fileStream);
        //    fileStream.Close();

        //    var ext = format;

        //    Stream fileStreambanners = stream;

        //    fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
        //    string videoURL = UploadImage1(fileNamebanners, path, ext, fileStreambanners);
        //    if (System.IO.File.Exists(path))
        //    {
        //        System.IO.File.Delete(path);

        //    }
        //    return videoURL;
        //}

        //public string UploadImage1(string fileName, string path, string ext, Stream InputStream)
        //{

        //    string account = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountName"].ToString();
        //    string key = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountKey"].ToString();


        //    CloudMediaContext context = new CloudMediaContext(account, key);

        //    var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);

        //    var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));


        //    //var accessPolicy1 = context.AccessPolicies.Create(fileName, TimeSpan.FromDays(3), AccessPermissions.Write | AccessPermissions.List);

        //    //  var locator1 = context.Locators.CreateLocator(LocatorType.Sas, uploadAsset, accessPolicy1);
        //    //Convert.ToString(Path.GetFileNameWithoutExtension(uploadAsset.Name)).ToUpper()
        //    assetFile.Upload(path);
        //    //  locator1.Delete();
        //    // accessPolicy1.Delete();


        //    //var encodeAssetId = uploadAsset.Id; // "YOUR ASSET ID";
        //    //// Preset reference documentation: http://msdn.microsoft.com/en-us/library/windowsazure/jj129582.aspx
        //    //var encodingPreset = "H264 Broadband 720p";
        //    //var assetToEncode = context.Assets.Where(a => a.Id == encodeAssetId).FirstOrDefault();
        //    //if (assetToEncode == null)
        //    //{
        //    //    throw new ArgumentException("Could not find assetId: " + encodeAssetId);
        //    //}

        //    //IJob job = context.Jobs.Create("Encoding " + assetToEncode.Name + " to " + encodingPreset);

        //    //IMediaProcessor latestWameMediaProcessor = (from p in context.MediaProcessors where p.Name == "Azure Media Encoder" select p).ToList().OrderBy(wame => new Version(wame.Version)).LastOrDefault();
        //    //ITask encodeTask = job.Tasks.AddNew("Encoding", latestWameMediaProcessor, encodingPreset, TaskOptions.None);
        //    //encodeTask.InputAssets.Add(assetToEncode);
        //    //encodeTask.OutputAssets.AddNew(assetToEncode.Name + " as " + encodingPreset, AssetCreationOptions.None);

        //    //job.StateChanged += new EventHandler<JobStateChangedEventArgs>((sender, jsc) => Console.WriteLine(string.Format("{0}\n  State: {1}\n  Time: {2}\n\n", ((IJob)sender).Name, jsc.CurrentState, DateTime.UtcNow.ToString(@"yyyy_M_d_hhmmss"))));
        //    //job.Submit();
        //    //job.GetExecutionProgressTask(CancellationToken.None).Wait();

        //    //var preparedAsset = job.OutputMediaAssets.FirstOrDefault();



        //    var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
        //    var daysForWhichStreamingUrlIsActive = 365;
        //    var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
        //    var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive),
        //                                             AccessPermissions.Read);
        //    string streamingUrl = string.Empty;
        //    var assetFiles = streamingAsset.AssetFiles.ToList();
        //    //var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith("m3u8-aapl.ism")).FirstOrDefault();
        //    //var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().Equals(streamingAsset.Name.ToLower())).FirstOrDefault();
        //    //if (streamingAssetFile != null)
        //    //{
        //    //    var locator = context.Locators.CreateLocator(LocatorType.OnDemandOrigin, streamingAsset, accessPolicy);
        //    //    Uri hlsUri = new Uri(locator.Path + streamingAssetFile.Name + "/manifest(format=m3u8-aapl)");
        //    //    streamingUrl = hlsUri.ToString();
        //    //}
        //    //streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".ism")).FirstOrDefault();
        //    //if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
        //    //{
        //    //    var locator = context.Locators.CreateLocator(LocatorType.OnDemandOrigin, streamingAsset, accessPolicy);
        //    //    Uri smoothUri = new Uri(locator.Path + streamingAssetFile.Name + "/manifest");
        //    //    streamingUrl = smoothUri.ToString();
        //    //}
        //    var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".mp4")).FirstOrDefault();
        //    if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
        //    {
        //        var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
        //        var mp4Uri = new UriBuilder(locator.Path);
        //        mp4Uri.Path += "/" + streamingAssetFile.Name;
        //        streamingUrl = mp4Uri.ToString();
        //    }
        //    return streamingUrl;

        //}

    }
}
