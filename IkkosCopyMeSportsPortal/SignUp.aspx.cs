﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;
using IkkosMultiSportsCommon;
using Microsoft.WindowsAzure.MediaServices.Client;


namespace IkkosCopyMeSportsPortal
{
    public partial class SignUp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }

            CustomResponse result = APICalls.Get("Sports/GetSports");
            if (result.Response != null)
            {
                JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                serializer2.MaxJsonLength = 1000000000;
                var sports = result.Response.ToString();

                List<GetSportsDTO> sportsdata = serializer2.Deserialize<List<GetSportsDTO>>(sports);
                var SportsJson = new JavaScriptSerializer().Serialize(sportsdata);

                DataTable SportsTable = JsonStringToDataTable(SportsJson);
                //SportsTable.Rows.Add("Select Sport", "Select Sport");
                DataView view = SportsTable.DefaultView;
                view.Sort = "SportName";
                ddlSport.DataSource = view;
                ddlSport.DataValueField = "SportId";
                ddlSport.DataTextField = "SportName";
                ddlSport.DataBind();

            }
            else
            {

            }
            CustomResponse result1 = APICalls.Get("Package/GetPricingCatalog");
            if (result1.Response != null)
            {
                JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                serializer2.MaxJsonLength = 1000000000;
                var sports = result1.Response.ToString();

                List<PricingCatalogDTO> PricingData = serializer2.Deserialize<List<PricingCatalogDTO>>(sports);
                var PricingJson = new JavaScriptSerializer().Serialize(PricingData);

                DataTable PackagePricingTable = JsonStringToDataTable(PricingJson);
                DataRow[] dr = PackagePricingTable.Select("PricingType=1");
                DataTable newTable = new DataTable();
                newTable = PackagePricingTable.Clone();
                foreach (DataRow row in dr)
                {
                    newTable.ImportRow(row);
                }

                DataView view = newTable.DefaultView;
                view.Sort = "Id";
                ddlPricing.DataSource = view;
                ddlPricing.DataValueField = "Id";
                ddlPricing.DataTextField = "Price";
                ddlPricing.DataBind();
            }

            else
            {

            }
        }

        public DataTable JsonStringToDataTable(string jsonString)
        {
            DataTable dt = new DataTable();
            string[] jsonStringArray = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{");
            List<string> ColumnsName = new List<string>();
            foreach (string jSA in jsonStringArray)
            {
                string[] jsonStringData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                foreach (string ColumnsNameData in jsonStringData)
                {
                    try
                    {
                        int idx = ColumnsNameData.IndexOf(":");
                        string ColumnsNameString = ColumnsNameData.Substring(0, idx - 1).Replace("\"", "");
                        if (!ColumnsName.Contains(ColumnsNameString))
                        {
                            ColumnsName.Add(ColumnsNameString);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Error Parsing Column Name : {0}", ColumnsNameData));
                    }
                }
                break;
            }
            foreach (string AddColumnName in ColumnsName)
            {
                dt.Columns.Add(AddColumnName);
            }
            foreach (string jSA in jsonStringArray)
            {
                string[] RowData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                DataRow nr = dt.NewRow();
                foreach (string rowData in RowData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        string RowColumns = rowData.Substring(0, idx - 1).Replace("\"", "");
                        string RowDataString = rowData.Substring(idx + 1).Replace("\"", "");
                        nr[RowColumns] = RowDataString;
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
                dt.Rows.Add(nr);
            }
            return dt;
        }

        protected void btnSignin_Click(object sender, EventArgs e)
        {
            HttpPostedFile file = Request.Files["PPUrl"];
            HttpPostedFile file1 = Request.Files["CVUrl"];
            string password1 = txtPwd.Text;
            string password2 = txtCnfrmPwd.Text;

            if (password1 == password2 && PPUrl != null)
            {
                if (password1.Length >= 6)
                {
                    string profilePicURL = "";
                    using (MemoryStream ms = new MemoryStream())
                    {
                        string fileName = file.FileName;
                        ms.SetLength(file.ContentLength);
                        file.InputStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        string base64String = Convert.ToBase64String(ms.ToArray());
                        string format = file.ContentType.Split('/')[1];
                        profilePicURL = streamfile(ms, format);
                    }

                    string videoURL = "";
                    using (MemoryStream ms = new MemoryStream())
                    {
                        string fileName = file1.FileName;
                        ms.SetLength(file1.ContentLength);
                        file1.InputStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        string base64String = Convert.ToBase64String(ms.ToArray());
                        string format = file1.ContentType.Split('/')[1];
                        videoURL = streamfile(ms, format);
                    }

                    AdminUsersDTO objadminuser = new AdminUsersDTO();
                    objadminuser.Id = 0;
                    objadminuser.FirstName = txtFirstName.Text;
                    objadminuser.LastName = txtLastName.Text;
                    objadminuser.EmailAddress = txtEmail.Text;
                    objadminuser.Password = txtPwd.Text;
                    objadminuser.SportId = Int32.Parse(ddlSport.SelectedValue.ToString());
                    objadminuser.ChannelName = txtChannel.Text;
                    objadminuser.CreatedDate = DateTime.Now;
                    objadminuser.ProfilePicURL = profilePicURL;
                    objadminuser.VideoURL = videoURL;

                    string thumbnail = GetVideoThumbnails(objadminuser);
                    objadminuser.ThumbNailURL = thumbnail;
                    objadminuser.Description = txtDesc.Text;
                    objadminuser.PricingCatalogId = Convert.ToInt32(ddlPricing.SelectedItem.Value);
                    CustomResponse res = APICalls.Post("Users/UserRegistration", objadminuser);

                    //string status = res.Response.ToString();
                    //string status1 = "Successful";

                    if (res.Response != null)
                    {
                        Error.Text = res.Message;
                        Error.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        Error.Text = res.Message;
                        Error.ForeColor = System.Drawing.Color.Red;
                    }
                }
                else
                {

                    Error.Text = "Password Should Minimum of 6 characters in length!!!";
                    Error.ForeColor = System.Drawing.Color.Red;

                }
            }
            else
            {
                Error.Text = "Password MisMatch";
                Error.ForeColor = System.Drawing.Color.Red;
            }

            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtEmail.Text = "";
            txtChannel.Text = "";
            ddlSport.SelectedIndex = 0;
            txtDesc.Text = "";
            ddlPricing.SelectedIndex = 0;
        }


        private string streamfile(MemoryStream stream, string format)
        {

            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);

            var fileStream = System.IO.File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();

            var ext = format;

            Stream fileStreambanners = stream;


            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string fileURL = UploadFile(fileNamebanners, path, ext, fileStreambanners);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);

            }
            //finalVideoUrl = videoURL;
            return fileURL;
        }

        private string UploadFile(string fileName, string path, string ext, Stream InputStream)
        {

            string account = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountName"].ToString();
            string key = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountKey"].ToString();


            CloudMediaContext context = new CloudMediaContext(account, key);

            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);

            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));

            assetFile.Upload(path);


            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 365;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive),
                                                     AccessPermissions.Read);
            string streamingUrl = string.Empty;
            var assetFiles = streamingAsset.AssetFiles.ToList();
            var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith("." + ext)).FirstOrDefault();
            if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            {
                var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                var mp4Uri = new UriBuilder(locator.Path);
                mp4Uri.Path += "/" + streamingAssetFile.Name;
                streamingUrl = mp4Uri.ToString();
            }
            return streamingUrl;

        }
        private string GetVideoThumbnails(AdminUsersDTO AdminResp)
        {

            UploadImage uplImage = new UploadImage();
            ////Delete all Video thumbnails 
            //UsersDalV5.DeleteAthleteVideoThumbnails(videoResp.Id);

            var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
            int totDuration = AdminResp.Duration;
            float startPoint = 0;
            float dur = (float)totDuration / 6;

            var frame = startPoint + (float)Math.Round(2.0, 2);
            string thumbnailJpeGpath = HttpContext.Current.Server.MapPath("~/videoThumb" + 1 + ".jpg");
            ffMpeg.GetVideoThumbnail(AdminResp.VideoURL, thumbnailJpeGpath, frame);
            byte[] bytes = File.ReadAllBytes(thumbnailJpeGpath);
            string base64String = Convert.ToBase64String(bytes);
            string thumbnailUrl = uplImage.GetImageURL(base64String, ".jpg", "");

            //Add Image to the DB 

            startPoint = startPoint + (float)Math.Round(dur, 2);
            return thumbnailUrl;


        }
    }
}