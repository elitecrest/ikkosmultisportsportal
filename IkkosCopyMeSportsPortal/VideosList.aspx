﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VideosList.aspx.cs" Inherits="IkkosCopyMeSportsPortal.VideosList" EnableEventValidation="false" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1" />
    <title>Athletics App</title>
    <link rel="icon" href="img/favicon.png" type="image/png" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/linecons.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,900,700,700italic,400italic,300italic,300,100italic,100,900italic' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,500,700,800,600,300,200' rel='stylesheet' type='text/css' />
    <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->
    <script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/classie.js"></script>
    <script src="js/prefixfree.min.js"></script>
    <!--[if lt IE 9]> <script src="js/respond-1.1.0.min.js"></script> <script src="js/html5shiv.js"></script> <script src="js/html5element.js"></script><![endif]-->
    <script>
        $(document).ready(function () {
            $('#BulkUpload').change(function () {
                var fileCount = document.getElementById('BulkUpload').files.length;
                $('#lblFileList').text("You Have Selected " + fileCount + "File(s).");
            });
        });
    </script>
    <style>
        #dvSource {
            /*border: 5px solid #ccc;
            padding: 5px;
            min-height: 70px;
            width: 250px;
            position:relative;*/
            border: 4px solid #ccc;
            padding: 5px;
            margin: 0px 20px 20px 0;
            min-height: 220px;
            max-width: 245px;
            width: 100%;
            overflow: hidden;
        }

            #dvSource p {
                font-size: 13px !important;
                line-height: 16px !important;
                padding: 0;
                margin: 0;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="athletics-page">
                <!--Header_section-->
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" >
                                <img src="img/logo.png" /></a>
                        </div>
                        <ul class="nav navbar-nav">
                            <li><a href="Index.aspx">Home</a></li>
                            <li><a href="PackagesCreation.aspx">Package Creation</a></li>
                            <%--<li><a href="UploadVideo.aspx">Upload video</a></li>--%>
                            <li class="active"><a href="VideosList.aspx">Videos</a></li>
                            <li><a href="PackagesList.aspx">Packages List</a></li>
                            <li><a href="Login.aspx">Log Out</a></li>
                        </ul>
                    </div>
                </nav>
                <!--Header_section-->
                <!--Top_content-->
                <section id="top_content" class="inner_banner">
                    <div class="inner_banner_cont">
                        <div class="gradient"></div>
                        <div class="container">
                            <div class="inner_banner_ttl">
                                <div class="inner_banner_left_ttl delay-03s animated wow fadeInLeft">
                                    <h2>VIDEO <strong>LIST</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Top_content-->
                <!--Service-->
                <section id="uploads">
                    <div class="service_area">
                        <div class="row">
                            <div class="well well-lg">
                                <strong>Upload Videos:      </strong>
                                <label class="btn btn-primary">
                                    Browse&hellip;
                                   <%-- <input type="file" class="btn btn-primary" style="display: none;">--%>
                                    <asp:FileUpload ID="BulkUpload" runat="server" AllowMultiple="true" Style="display: none;" />
                                </label>
                                <asp:Button ID="UploadVideos" runat="server" Text="Upload" CssClass="btn btn-danger pull-right" OnClick="UploadVideos_Click" />
                                <asp:Label ID="lblFileList" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="service">
                    <div class="service_area">
                        <div class="row">
                            <div class="col-lg-12 col-xs-12">
                                <%-- <div class="well well-sm">
                                    <strong>Display</strong>
                                    <div class="btn-group"><a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a> <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>List</a> </div>
                                </div>--%>
                                <div id="products" class="row list-group">
                                    <asp:DataList ID="dtVideos" runat="server">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <th>Videos
                                                    </th>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="block" id="dvSource">
                                                <%-- <asp:Label ID="lblVideoName" runat="server" Text="<%# Eval("MomentName") %>"></asp:Label>--%>
                                                <video id="videoPlay" poster="<%# Eval("ThumbNailURL") %>"
                                                    controls="controls" width="232" height="232">
                                                    <source src="<%# Eval("VideoURL") %>" type="video/mp4" draggable="true" ondrag="func()1" />
                                                </video>
                                                <b>Video Name:</b>
                                                <p><asp:Label ID="lblCName" runat="server" Text='<%# Bind("MomentName") %>'></asp:Label></p>
                                                <br />
                                                <asp:Button ID="btnEdit" runat="server" Text="EDIT" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnDelete" runat="server" Text="DELETE" CssClass="btn btn-primary pull-right" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:DataList>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Service-->
                <!--twitter-feed-end-->
                <footer class="footer_section" id="contact">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            <div class="footer_bottom"><span>© 2016 Athletics training. All Rights Reserved.</span> </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="js/index.js"></script>
    </form>
</body>
</html>
