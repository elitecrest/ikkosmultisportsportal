﻿<script>
      function allowDrop(ev) {
          ev.preventDefault();
      }

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}
</script>

<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));

                $("#<%=lbldvDest0.ClientID %>").val(ui.draggable.attr('videoId'));

                // ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest video").length == 0) {
                    $("#dvDest").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest").append(ui.draggable);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest1").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest1.ClientID %>").val(ui.draggable.attr('videoId'));


                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest1 video").length == 0) {
                    $("#dvDest1").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest1").append(ui.draggable);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest2").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest2.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest2 video").length == 0) {
                    $("#dvDest2").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest2").append(ui.draggable);
            }
        });
    });
</script>


<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest3").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest3.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest3 video").length == 0) {
                    $("#dvDest3").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest3").append(ui.draggable);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest4").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest4.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest4 video").length == 0) {
                    $("#dvDest4").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest4").append(ui.draggable);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest5").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest5.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest5 video").length == 0) {
                    $("#dvDest5").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest5").append(ui.draggable);
            }
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest6").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest6.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest6 video").length == 0) {
                    $("#dvDest6").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest6").append(ui.draggable);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest7").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest7.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest7 video").length == 0) {
                    $("#dvDest7").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest7").append(ui.draggable);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest8").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest8.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest8 video").length == 0) {
                    $("#dvDest8").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest8").append(ui.draggable);
            }
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest9").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest9.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest9 video").length == 0) {
                    $("#dvDest9").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest9").append(ui.draggable);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest10").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest10.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest10 video").length == 0) {
                    $("#dvDest10").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest10").append(ui.draggable);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest11").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest11.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest11 video").length == 0) {
                    $("#dvDest11").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest11").append(ui.draggable);
            }
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest12").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest12.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest12 video").length == 0) {
                    $("#dvDest12").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest12").append(ui.draggable);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest13").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest13.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest13 video").length == 0) {
                    $("#dvDest13").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest13").append(ui.draggable);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#dvSource video").draggable({
            revert: "invalid",
            refreshPositions: true,
            drag: function (event, ui) {
                ui.helper.addClass("draggable");
                console.log("drag called added draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");
                console.log("stop called removed draggable");
            }
        });
        $("#dvDest14").droppable({
            drop: function (event, ui) {
                console.log("dvDest drop called");
                console.log("video source=" + ui.draggable.attr('src'));
                var draggedVideoUrl = ui.draggable.attr('src')
                var draggedVideoId = ui.draggable.attr('videoId');

                //var videoInfoDto = {
                //    "PreInstructionVideoId": draggedVideoId
                //    ,


                //}

                console.log("custom video Id=" + ui.draggable.attr('videoId'));
                $("#<%=lbldvDest14.ClientID %>").val(ui.draggable.attr('videoId'));

                ui.draggable.draggable("option", "revert", false);
                if ($("#dvDest14 video").length == 0) {
                    $("#dvDest14").html("");
                }
                ui.draggable.addClass("dropped");
                $(this).droppable("destroy");
                $("#dvDest14").append(ui.draggable);
            }
        });
    });
</script>