﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="IkkosCopyMeSportsPortal.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1">
    <title>Athletics App</title>
    <link rel="icon" href="img/favicon.png" type="image/png">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/linecons.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,900,700,700italic,400italic,300italic,300,100italic,100,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,500,700,800,600,300,200' rel='stylesheet' type='text/css'>
    <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->
    <script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/classie.js"></script>
    <!--[if lt IE 9]> <script src="js/respond-1.1.0.min.js"></script> <script src="js/html5shiv.js"></script> <script src="js/html5element.js"></script><![endif]-->
</head>
<body>
    <div class="container">
        <div class="athletics-page">
            <!--Header_section-->
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header"><a class="navbar-brand">
                        <img src="img/logo.png" /></a> </div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="Index.aspx">Home</a></li>
                        <li><a href="PackagesCreation.aspx">Package Creation</a></li>
                       <%-- <li><a href="UploadVideo.aspx">Upload video</a></li>--%>
                        <li><a href="VideosList.aspx">Videos</a></li>
                        <li><a href="PackagesList.aspx">Packages List</a></li>
                        <li><a href="Login.aspx"> Log Out</a></li>
                     <%--   <li><a href="Login.aspx">Login</a></li>--%>
                    </ul>
                </div>
            </nav>
            <!--Header_section-->
            <!--Top_content-->
            <section id="top_content" class="top_cont_outer">
                <div class="top_cont_inner">
                    <div class="gradient"></div>
                    <div class="container">
                        <div class="top_content">
                            <div class="top_left_cont delay-03s animated wow zoomIn">
                                <h2>ATHLETICS TRANING <strong>APP</strong></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Top_content-->
            <!--Service-->
            <section id="service">
                <h2 class="delay-03s animated wow zoomIn">ABOUT THE ATHLETICS </h2>
                <div class="service_area">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12 animated fadeInUp wow text-center" style="text-align: justify !important;">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                    </div>
                </div>
            </section>
            <!--Service-->
            <section id="work_outer">
                <h2 class="animated zoomIn wow">Our APP SCREENS</h2>
                <div class="work_section">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12 animated fadeInUp wow">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel" data-slide-to="1"></li>
                                    <li data-target="#myCarousel" data-slide-to="2"></li>
                                    <li data-target="#myCarousel" data-slide-to="3"></li>
                                </ol>
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <img src="img/app.jpg" class="img-responsive" />
                                    </div>
                                    <div class="item">
                                        <img src="img/app2.jpg" class="img-responsive" />
                                    </div>
                                    <div class="item">
                                        <img src="img/app3.jpg" class="img-responsive" />
                                    </div>
                                    <div class="item">
                                        <img src="img/app4.jpg" class="img-responsive" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--main-section-end-->
            <!--twitter-feed-end-->
            <footer class="footer_section" id="contact">
                <div class="row">
                    <div class="col-lg-12 col-xs-12">
                        <div class="footer_bottom"><span>© 2016 Athletics training. All Rights Reserved.</span> </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
</html>
