﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
namespace IkkosCopyMeSportsPortal
{
    public partial class test2 : System.Web.UI.Page
    {
        public static SqlConnection ConnectTodb()
        {

            string connectionString = ConfigurationManager.ConnectionStrings["LemonaidDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection myConn = ConnectTodb();
            StringBuilder query = new StringBuilder();
            query.Append(@"Select VideoURL from videos where id in(112,113,114,115,116)");
            using (SqlCommand cmd = new SqlCommand(query.ToString()))
            //using (SqlCommand cmd = new SqlCommand("select * from AtheleteInstituteMatches  where  Convert(varchar,CreatedDate,101) BETWEEN '02/01/2016'  AND '02/14/2016' and CollAtheleteCoachType = 1"))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = myConn;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        dlProducts.DataSource = dt;
                        dlProducts.RepeatColumns = 2;
                        dlProducts.RepeatDirection = RepeatDirection.Horizontal;
                        dlProducts.DataBind();
                    }
                }
            }
            myConn.Close();
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {

        }
    }
}