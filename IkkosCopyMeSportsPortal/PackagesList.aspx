﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PackagesList.aspx.cs" Inherits="IkkosCopyMeSportsPortal.PackagesList" EnableEventValidation="false" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <style type="text/css">
        .label {
            display: inline;
        }

        .textbox {
            display: inline;
        }
    </style>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1" />
    <title>Athletics App</title>
    <link rel="icon" href="img/favicon.png" type="image/png" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/linecons.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,900,700,700italic,400italic,300italic,300,100italic,100,900italic' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,500,700,800,600,300,200' rel='stylesheet' type='text/css' />
    <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->
    <script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/classie.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
    <script type="text/javascript">jQuery(document).ready(function ($) { tab = $('.tabs h3 a'); tab.on('click', function (event) { event.preventDefault(); tab.removeClass('active'); $(this).addClass('active'); tab_content = $(this).attr('href'); $('div[id$="tab-content"]').removeClass('active'); $(tab_content).addClass('active'); }); });</script>
    <!--[if lt IE 9]> <script src="js/respond-1.1.0.min.js"></script> <script src="js/html5shiv.js"></script> <script src="js/html5element.js"></script><![endif]-->
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }

        /*img {
            height: 100px;
            width: 100px;
            margin: 2px;
        }*/

        .draggable {
            filter: alpha(opacity=60);
            opacity: 0.6;
        }

        .dropped {
            position: static !important;
        }

        .dvBind {border:3px solid #ccc; padding:10px; margin-right:15px; margin-bottom:15px; min-height:350px; max-width:400px; position:relative; }
        .dvBind video {max-width:290px; min-height:220px;
        } 

        #dvEdit {
            border:3px solid #ccc; padding:10px; margin-right:15px; margin-bottom:15px; min-height:350px; max-width:400px; position:relative;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="athletics-page">
                <!--Header_section-->
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand">
                                <img src="img/logo.png" /></a>
                        </div>
                        <ul class="nav navbar-nav">
                            <li><a href="Index.aspx">Home</a></li>
                            <li><a href="PackagesCreation.aspx">Package Creation</a></li>
                            <%--<li><a href="UploadVideo.aspx">Upload video</a></li>--%>
                            <li><a href="VideosList.aspx">Video</a></li>
                            <li class="active"><a href="PackagesList.aspx">Packages List</a></li>
                            <li><a href="Login.aspx">Log Out</a></li>
                            
                        </ul>
                    </div>
                </nav>
                <!--Header_section-->
                <!--Top_content-->
                <section id="top_content" class="inner_banner">
                    <div class="inner_banner_cont">
                        <div class="gradient"></div>
                        <div class="container">
                            <div class="inner_banner_ttl">
                                <div class="inner_banner_left_ttl delay-03s animated wow fadeInLeft">
                                    <h2>PACKAGES <strong>LIST</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Top_content-->
                <!--Service-->
                <section id="uploads">
                    <div class="service_area">
                        <div class="row">
                            <%--<div class="well well-lg">
                                <strong>Upload Videos:      </strong>
                                <label class="btn btn-primary">
                                    Browse&hellip;
                                  
                                    <asp:FileUpload ID="BulkUpload" runat="server" AllowMultiple="true" Style="display: none;" />
                                </label>
                                <asp:Button ID="UploadVideos" runat="server" Text="Upload" CssClass="btn btn-danger pull-right" OnClick="UploadVideos_Click" />
                                <asp:Label ID="lblFileList" runat="server"></asp:Label>
                            </div>--%>
                        </div>
                    </div>
                </section>
                <section id="service">
                    <div class="service_area">
                        <div class="row">
                            <div class="col-lg-12 col-xs-12">
                                <%-- <div class="well well-sm">
                                    <strong>Display</strong>
                                    <div class="btn-group"><a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a> <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>List</a> </div>
                                </div>--%>
                                <div id="products" class="row list-group">
                                    <%--<asp:DataList ID="dtVideos" runat="server">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <th>Videos
                                                    </th>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="block" id="dvSource">
                                                <asp:Label ID="lblVideoName" runat="server" Text="<%# Eval("MomentName") %>"></asp:Label>
                                                <video id="videoPlay" poster="<%# Eval("ThumbNailURL") %>"
                                                    controls="controls" width="232" height="232">
                                                    <source src="<%# Eval("VideoURL") %>" type="video/mp4" draggable="true" />
                                                </video>
                                                <asp:Button ID="btnEdit" runat="server" Text="EDIT" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnDelete" runat="server" Text="DELETE" CssClass="btn btn-primary pull-right" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:DataList>--%>
                                    <asp:Label ID="lblNoData" runat="server"></asp:Label>
                                    <asp:DataList ID="dtPackages" runat="server" OnItemCommand="dtPackages_ItemCommand">
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="dvBind">
                                                PackageName:<asp:Label ID="lblsample" runat="server" Text='<%# Bind("PackageName") %>'></asp:Label>
                                                <br />
                                                <%-- <div>
                                                    PackageDescription:<asp:Label ID="Label1" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                                </div>--%>
                                                &nbsp;&nbsp;<video id="videoPlay" poster="<%# Eval("ThumbNailURL") %>" controls="controls">
                                                    <source src="<%# Eval("VideoURL") %>" type="video/mp4" draggable="true" />
                                                </video>
                                                <br />
                                                <asp:Label ID="lblPricing" runat="server" Text='<%# Bind("PricingCatalogId") %>' Visible="false"></asp:Label>
                                                <br />
                                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnEdit" runat="server" CommandName="cmd_Edit" class="btn btn-info" PostBackUrl='<%# "~/EditPackage.aspx?PackageId=" + Eval("PackageId") %>'>Edit</asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" class="btn btn-info">Delete</asp:LinkButton>
                                                <br />
                                                <div class="clearfix"></div>
                                            </div>
                                        </ItemTemplate>
                                        <%--<EditItemTemplate>
                                            <div class="Edit">
                                                <p>Package Name :</p>
                                                <asp:TextBox ID="txtPName" runat="server" Text='<%# Eval("PackageName")%>'></asp:TextBox>
                                                <p>Package Description :</p>
                                                <asp:TextBox ID="txt_surname" runat="server" Text='<%# Eval("Description")%>'></asp:TextBox>
                                                <p>Package Pricing :</p>
                                                <asp:DropDownList ID="ddlPricing" runat="server"></asp:DropDownList>
                                                <p>
                                                    <video id="videoPlay" poster="<%# Eval("ThumbNailURL") %>"
                                                        controls="controls" width="180" height="180">
                                                        <source src="<%# Eval("VideoURL") %>" type="video/mp4" draggable="true" />
                                                    </video>
                                                </p>
                                                <label>
                                                    <asp:LinkButton ID="btn_Update" runat="server" CommandName="cmd_Update" CommandArgument='<%# Eval("PackageId")%>'>Update</asp:LinkButton>
                                                </label>
                                                <label>
                                                    <asp:LinkButton ID="btn_cancel" runat="server" CommandName="cmd_Cancel">Cancel</asp:LinkButton>
                                                </label>
                                        </EditItemTemplate>--%>
                                        <EditItemTemplate>
                                            <div class="block" id="dvEdit">
                                                <asp:Image ID="imgEmp" runat="server" Width="180px" Height="120px" ImageUrl='<%# Eval("ThumbNailURL") %>' Style="padding-left: 40px" />
                                                <br />
                                                <asp:Label ID="lblEditPackageId" runat="server" Visible="false" Text='<%# Bind("PackageId") %>'></asp:Label>
                                                <br />
                                                <b>Package Name:</b>
                                                <%-- <asp:Label ID="lblCName" runat="server" Text='<%# Bind("PackageName") %>'></asp:Label>--%>
                                                <asp:TextBox ID="txtPName" runat="server" Text='<%# Eval("PackageName")%>'></asp:TextBox>
                                                <br />
                                                <br />
                                                <b>Package Desc:</b>
                                                <%--<asp:Label ID="lblName" runat="server" Text='<%# Bind("Description") %>'></asp:Label>--%>
                                                <asp:TextBox ID="txtDe" runat="server" Text='<%# Eval("Description")%>'></asp:TextBox>
                                                <br />
                                                <br />
                                                <b>Package Pricing:</b>
                                                <asp:DropDownList ID="ddl" runat="server" AppendDataBoundItems="true"></asp:DropDownList>
                                                <asp:Label ID="lblCatlog" runat="server" Visible="false" Text='<%# Bind("PricingCatalogId") %>'></asp:Label>
                                                <br />
                                                <br />
                                                <asp:LinkButton ID="btn_Update" runat="server" CommandName="cmd_Update" CommandArgument='<%# Eval("PackageId")%>' class="btn btn-info">Update</asp:LinkButton>
                                               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:LinkButton ID="btn_cancel" runat="server" CommandName="cmd_Cancel" class="btn btn-info">Cancel</asp:LinkButton>
                                            </div>
                                        </EditItemTemplate>
                                        <SelectedItemStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                    </asp:DataList>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Service-->
                <!--twitter-feed-end-->
                <footer class="footer_section" id="contact">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            <div class="footer_bottom"><span>© 2016 Athletics training. All Rights Reserved.</span> </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="js/index.js"></script>
    </form>
</body>
</html>
