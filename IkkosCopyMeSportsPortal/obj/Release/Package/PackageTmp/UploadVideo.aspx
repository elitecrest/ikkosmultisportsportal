﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadVideo.aspx.cs" Inherits="IkkosCopyMeSportsPortal.UploadVideo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1" />
    <title>Athletics App</title>
    <link rel="icon" href="img/favicon.png" type="image/png" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/linecons.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,900,700,700italic,400italic,300italic,300,100italic,100,900italic' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,500,700,800,600,300,200' rel='stylesheet' type='text/css' />
    <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->
    <script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/classie.js"></script>
    <!--[if lt IE 9]> <script src="js/respond-1.1.0.min.js"></script> <script src="js/html5shiv.js"></script> <script src="js/html5element.js"></script><![endif]-->
    <script>
        $(document).ready(function () {
            $('#BulkUpload').change(function () {
                var fileCount = document.getElementById('BulkUpload').files.length;
                $('#lblFileList').text("You Have Selected " + fileCount + "File(s).");
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="athletics-page">
                <!--Header_section-->
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="Login.aspx">
                                <img src="img/logo.png" /></a>
                        </div>
                        <ul class="nav navbar-nav">
                            <li><a href="Index.aspx">Home</a></li>
                            <li><a href="PackagesCreation.aspx">Package Creation</a></li>
                            <li class="active"><a href="UploadVideo.aspx">Upload video</a></li>
                            <li><a href="VideosList.aspx">Video list</a></li>
                            <li><a href="Login.aspx">Log Out</a></li>
                            <%--<li><a href="Login.aspx">Login</a></li>--%>
                        </ul>
                    </div>
                </nav>
                <!--Header_section-->
                <!--Top_content-->
                <section id="top_content" class="inner_banner">
                    <div class="inner_banner_cont">
                        <div class="gradient"></div>
                        <div class="container">
                            <div class="inner_banner_ttl">
                                <div class="inner_banner_left_ttl delay-03s animated wow fadeInLeft">
                                    <h2>UPLOAD<strong>VIDEO</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Top_content-->
                <!--Service-->
                <section id="service">
                    <div class="service_area">
                        <div class="row">
                            <div class="col-lg-12 col-xs-12">
                                <div class="col-lg-5 col-xs-5">
                                    <label class="btn btn-primary">
                                        Browse&hellip;
                                   <%-- <input type="file" class="btn btn-primary" style="display: none;">--%>
                                        <asp:FileUpload ID="BulkUpload" runat="server" AllowMultiple="true" Style="display: none;" />

                                    </label>
                                    <asp:Button ID="UploadVideos" runat="server" Text="Upload" CssClass="btn btn-danger pull-right" OnClick="UploadVideos_Click" />
                                    <asp:Label ID="lblFileList" runat="server"></asp:Label>
                                </div>
                                <div class="col-lg-5 col-xs-5 scrollbar" id="style-6" style="border-left: 1px solid #EBEBEB;">
                                    <div class="force-overflow">
                                        <%--<div class="row">
                                                <div class="col-xs-4 col-lg-4">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="img/video-banner.jpg" alt="" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-xs-8">
                                                    <div class="caption">
                                                        <h4 class="group inner list-group-item-heading">Product title</h4>
                                                        <p class="group inner list-group-item-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-6">Left Text</div>
                                                            <div class="col-xs-12 col-md-6">Right Text</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-4 col-lg-4">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="img/video-banner.jpg" alt="" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-xs-8">
                                                    <div class="caption">
                                                        <h4 class="group inner list-group-item-heading">Product title</h4>
                                                        <p class="group inner list-group-item-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-6">Left Text</div>
                                                            <div class="col-xs-12 col-md-6">Right Text</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-4 col-lg-4">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="img/video-banner.jpg" alt="" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-xs-8">
                                                    <div class="caption">
                                                        <h4 class="group inner list-group-item-heading">Product title</h4>
                                                        <p class="group inner list-group-item-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-6">Left Text</div>
                                                            <div class="col-xs-12 col-md-6">Right Text</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-4 col-lg-4">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="img/video-banner.jpg" alt="" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-xs-8">
                                                    <div class="caption">
                                                        <h4 class="group inner list-group-item-heading">Product title</h4>
                                                        <p class="group inner list-group-item-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-6">Left Text</div>
                                                            <div class="col-xs-12 col-md-6">Right Text</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-4 col-lg-4">
                                                    <div class="thumbnail">
                                                        <img class="img-responsive" src="img/video-banner.jpg" alt="" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-xs-8">
                                                    <div class="caption">
                                                        <h4 class="group inner list-group-item-heading">Product title</h4>
                                                        <p class="group inner list-group-item-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-6">Left Text</div>
                                                            <div class="col-xs-12 col-md-6">Right Text</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>--%>

                                        <%--<asp:DataList ID="DataList1" runat="server" BackColor="Gray" BorderColor="#666666"
                                            BorderStyle="None" BorderWidth="2px" CellPadding="3" CellSpacing="2"
                                            Font-Names="Verdana" Font-Size="Small" GridLines="Both" RepeatColumns="3" RepeatDirection="Vertical"
                                            Width="600px">
                                            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                            <HeaderStyle BackColor="#333333" Font-Bold="True" Font-Size="Large" ForeColor="White"
                                                HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                Uploaded Videos
                                            </HeaderTemplate>
                                            <ItemStyle BackColor="White" ForeColor="Black" BorderWidth="2px" />
                                            <ItemTemplate>
                                                <asp:Image ID="imgEmp" runat="server" Width="100px" Height="120px" ImageUrl='<%# Bind("PhotoPath", "~/photo/{0}") %>' Style="padding-left: 40px" /><br />
                                                <b>Video Name:</b>
                                                <asp:Label ID="lblCName" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                                                <br />
                                                <b>Moment Name:</b>
                                                <asp:Label ID="lblName" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                                                <br />
                                                <b>Sport:</b>
                                                <asp:Label ID="lblCity" runat="server" Text=' <%# Bind("City") %>'></asp:Label>
                                                <br />
                                                <b>Channel Name:</b>
                                                <asp:Label ID="lblCountry" runat="server" Text='<%# Bind("Country") %>'></asp:Label>
                                                <br />
                                            </ItemTemplate>
                                            <ItemTemplate>
                                                <a id="aVid1" href="javascript:ShowMyModalPopup('<%#Eval("PlayUrl") %>');">
                                                    <img src='<%#Eval("Url") %>' width="200px" height="80px" /></a><br />
                                                <a id="a1" href="javascript:ShowMyModalPopup('<%#Eval("PlayUrl") %>');">
                                                    <%#Eval("Title")+"(" +Eval("Duration")+")"%></a>
                                                <br />
                                            </ItemTemplate>
                                        </asp:DataList>--%>
                                        <div class="col-lg-8 col-xs-8">
                                            <div class="caption">
                                                <asp:GridView ID="gd" runat="server" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="video">
                                                            <ItemTemplate>
                                                                <h1>SAMPLE VIDEOS</h1>
                                                                <div class="smallText">Note:This Is the Video Data from lemonaid Swimming</div>
                                                                <p>
                                                                    <video id="videoPlay" poster="/images/videoposter.jpg"
                                                                        controls="controls" width="400" height="400">
                                                                        <source src="<%# Eval("VideoURL") %>" type="video/mp4" />
                                                                    </video>
                                                                </p>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Service-->
                <!--twitter-feed-end-->
                <footer class="footer_section" id="contact">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            <div class="footer_bottom"><span>© 2016 Athletics training. All Rights Reserved.</span> </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </form>
</body>
</html>
