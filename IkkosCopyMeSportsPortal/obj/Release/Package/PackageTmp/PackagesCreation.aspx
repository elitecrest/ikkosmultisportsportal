﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PackagesCreation.aspx.cs" Inherits="IkkosCopyMeSportsPortal.PackagesCreation" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1" />
    <title>Athletics App</title>
    <link rel="icon" href="img/favicon.png" type="image/png" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/linecons.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,900,700,700italic,400italic,300italic,300,100italic,100,900italic' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,500,700,800,600,300,200' rel='stylesheet' type='text/css' />
    <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->
    <script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/classie.js"></script>
    <!--[if lt IE 9]> <script src="js/respond-1.1.0.min.js"></script> <script src="js/html5shiv.js"></script> <script src="js/html5element.js"></script><![endif]-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.8.24/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://code.jquery.com/ui/1.8.24/themes/blitzer/jquery-ui.css" rel="stylesheet"
        type="text/css" />
    <script>
        $(".block").draggable({ helper: 'clone' });

        $("#shoppingCart").droppable(
        {
            accept: ".block",
            drop: function (ev, ui) {
            }
        });

    </script>
    <%--<style>
        #txtTraingingVideos {
            border-radius: 25px;
            border: 2px solid #CCCCCC;
            padding: 20px;
            width: 140px;
            height: 150px;
        }

        #txtInstVideos {
            border-radius: 25px;
            border: 2px solid #CCCCCC;
            padding: 20px;
            width: 140px;
            height: 150px;
        }
    </style>--%>
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }

        img {
            height: 100px;
            width: 100px;
            margin: 2px;
        }

        .draggable {
            filter: alpha(opacity=60);
            opacity: 0.6;
        }

        .dropped {
            position:static !important;  
        }

        #dvSource {
            border: 5px solid #ccc;
            padding: 5px;
            min-height: 180px;
            width: 215px;
        }

        #dvDest, #dvDest1, #dvDest2 {
            border: 3px solid #ccc;
            padding: 5px;
            min-height: 180px;
            width: 140px;
        }
    </style>
    <script>
        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
        }

        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
        }
    </script>
    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                    // var image = this.src.split("/")[this.src.split("/").length - 1];
                    // var image = this;//.split("/")[this.src.split("/").length - 1];

                    if ($.ui.ddmanager.drop(ui.helper.data("draggable"), event)) {

                        var dlId = document.getElementById("lblVideoName");
                        console.log("about to get videoplay");

                        var x = document.getElementById("videoPlay").src;
                        $("#lbldvDest").html = x;
                        //alert(x);
                        // $("#test").append(x);
                        //  $("#test").html(x);
                       // alert(x + " dropped.");

                    }
                    else {
                        //alert(image + " not dropped.");
                    }
                }
            });
            $("#dvDest").droppable({
                drop: function (event, ui) {

                    if ($("#dvDest video").length == 0) {
                        $("#dvDest").html("");
                    }

                    ui.draggable.addClass("dropped");
                    $("#dvDest").append(ui.draggable);
                }
            });
        });

    </script>
    <script>
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                    // var image = this.src.split("/")[this.src.split("/").length - 1];
                    // var image = this;//.split("/")[this.src.split("/").length - 1];

                    if ($.ui.ddmanager.drop(ui.helper.data("draggable"), event)) {

                        var dlId = document.getElementById("lblVideoName");
                        console.log("about to get videoplay");

                        var x = document.getElementById("videoPlay").src;
                        $("#lbldvDest1").html = x;
                        // $("#test").append(x);
                        //  $("#test").html(x);
                        

                    }
                    else {
                        //alert(image + " not dropped.");
                    }
                }
            });
            $("#dvDest1").droppable({
                drop: function (event, ui) {

                    if ($("#dvDest1 video").length == 0) {
                        $("#dvDest1").html("");
                    }

                    ui.draggable.addClass("dropped");
                    $("#dvDest1").append(ui.draggable);
                }
            });
        });
    </script>
    <script>
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                    // var image = this.src.split("/")[this.src.split("/").length - 1];
                    // var image = this;//.split("/")[this.src.split("/").length - 1];

                    if ($.ui.ddmanager.drop(ui.helper.data("draggable"), event)) {

                        var dlId = document.getElementById("lblVideoName");
                        console.log("about to get videoplay");

                        var x = document.getElementById("videoPlay").src;
                        //alert(x);
                        $("#lbldDest2").html = x;
                        //alert(x);
                        // $("#test").append(x);
                        //  $("#test").html(x);
                        //alert(x + " dropped.");

                    }
                    else {
                        //alert(image + " not dropped.");
                    }
                }
            });
            $("#dvDest2").droppable({
                drop: function (event, ui) {

                    if ($("#dvDest2 video").length == 0) {
                        $("#dvDest2").html("");
                    }

                    ui.draggable.addClass("dropped");
                    $("#dvDest2").append(ui.draggable);
                }
            });
        });
    </script>
</head>
<body>
    <form id="Form1" runat="server">
        <div class="container">
            <div class="athletics-page">
                <!--Header_section-->
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand">
                                <img src="img/logo.png" /></a>
                        </div>
                        <ul class="nav navbar-nav">
                            <li><a href="Index.aspx">Home</a></li>
                            <li class="active"><a href="PackagesCreation.aspx">Package Creation</a></li>
                            <%-- <li><a href="UploadVideo.aspx">Upload video</a></li>--%>
                            <li><a href="VideosList.aspx">Video list</a></li>
                            <li><a href="Login.aspx">Log Out</a></li>
                            <%--  <li><a href="Login.aspx">Login</a></li>--%>
                        </ul>
                    </div>
                </nav>
                <!--Header_section-->
                <!--Top_content-->
                <section id="top_content" class="inner_banner">
                    <div class="inner_banner_cont">
                        <div class="gradient"></div>
                        <div class="container">
                            <div class="inner_banner_ttl">
                                <div class="inner_banner_left_ttl delay-03s animated wow fadeInLeft">
                                    <h2>Packages <strong>Creation</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Top_content-->
                <!--Service-->
                <section id="service">
                    <div class="service_area">
                        <div class="row">
                            <%--<div class="col-lg-6 col-xs-6">
                                <div class="col-lg-6 col-xs-6">
                                    <div class="price-box to-animate">
                                        <h2 class="pricing-plan">Starter</h2>
                                        <div class="price"><sup class="currency">$</sup>7</div>
                                        <p>Basic customer support for small business</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <div class="price-box to-animate">
                                        <h2 class="pricing-plan">Regular</h2>
                                        <div class="price"><sup class="currency">$</sup>19</div>
                                        <p>Basic customer support for small business</p>
                                    </div>
                                </div>
                                <div class="clearfix visible-sm-block"></div>
                                <div class="col-lg-6 col-xs-6 to-animate">
                                    <div class="price-box popular">
                                        <h2 class="pricing-plan">Plus</h2>
                                        <div class="price"><sup class="currency">$</sup>79</div>
                                        <p>Basic customer support for small business</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <div class="price-box to-animate">
                                        <h2 class="pricing-plan">Enterprise</h2>
                                        <div class="price"><sup class="currency">$</sup>125</div>
                                        <p>Basic customer support for small business</p>
                                    </div>
                                </div>
                            </div>--%>
                            <div class="col-lg-6 col-xs-6">
                                <div class="formcontent text-center to-animate">
                                    <form class="contact-form">
                                        <h2>PACKAGES CREATION</h2>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtPackageName" runat="server" placeholder="Package Name" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvUser" ErrorMessage="Please enter PackageName" ControlToValidate="txtPackageName" runat="server" ForeColor="Red" />
                                            <%-- <input type="name" class="form-control" placeholder="Package Name">--%>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtDescription" runat="server" placeholder="Description" CssClass="form-control" Rows="7" TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please enter PackageName" ControlToValidate="txtDescription" runat="server" ForeColor="Red" />
                                            <%--<textarea class="form-control" id="message" rows="7" placeholder="Description"></textarea>--%>
                                        </div>
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddlPricing" runat="server" Height="30px" Width="430px" CssClass="textbox" AppendDataBoundItems="true">
                                                <asp:ListItem Value="SelectSport">-- Select Package Pricing-- </asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblIntro" runat="server" Font-Size="13"
                                                Text="Introduction Video" Width="325"></asp:Label>
                                            <%--<input type="email" class="form-control" placeholder="Intro Video">--%>
                                            <asp:FileUpload ID="up1" runat="server" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ErrorMessage="Introduction Video Required" ControlToValidate="up1"
                                                runat="server" Display="Dynamic" ForeColor="Red" />
                                        </div>
                                        <div class="form-group">
                                            <p>
                                                <span style="float: left; font-size: 10pt">PreInstruction Videos:</span>
                                                <span style="float: initial; font-size: 10pt">Training Video:</span>
                                                <span style="float: right; font-size: 10pt">Post Instruction Video:</span>
                                            </p>
                                            <br />
                                            <br />
                                            <%--<p>
                                                <span style="float: right">
                                                    <asp:TextBox ID="txtTraingingVideos" runat="server" CssClass="form-control" Rows="7" TextMode="MultiLine" Style="resize: none"></asp:TextBox></span>
                                                <span style="float: left">
                                                    <asp:TextBox ID="txtInstVideos" runat="server" CssClass="form-control" Rows="7" TextMode="MultiLine" Style="resize: none"></asp:TextBox></span>
                                            </p>--%>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="width: 33%;">
                                                        <div id="dvDest">
                                                            Drop here
           <asp:Label ID="lbldvDest" runat="server" Visible="false"></asp:Label>
                                                        </div>
                                                    </td>
                                                    <td style="width: 33%;">
                                                        <div id="dvDest1">
                                                            Drop here
           <asp:Label ID="lbldvDest1" runat="server" Visible="false"></asp:Label>
                                                        </div>
                                                    </td>
                                                    <td style="width: 33%;">
                                                        <div id="dvDest2">
                                                 <asp:Label ID="lbldDest2" runat="server" Visible="false"></asp:Label>           Drop here
           
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>

                                            <br />
                                           
                                        </div>
                                        <br />
                                        <br />
                                        <div>
                                            <asp:Button ID="Button1" runat="server" Text="Create Package" CssClass="btn btn-danger" OnClick="btnCreate_Click" />
                                        </div>
                                        <div>
                                            <asp:Label ID="Error" runat="server"></asp:Label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-6">
                                <div class="formcontent text-center to-animate">
                                    <form class="contact-form">
                                        <h2>VIDEOS LIST</h2>
                                        <asp:DataList ID="dtVideos" runat="server">
                                            <HeaderTemplate>
                                                <table style="width: 100%">
                                                    <tr>
                                                        <th>Videos
                                                        </th>

                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div id="dvSource">
                                                    <%-- <video id="videoPlay" poster="/images/videoposter.jpg"
                        controls="controls" width="168" height="168">
                        <source src="<%# Eval("VideoURL") %>" type="video/mp4" draggable="true" />
                    </video>--%>
                                                    <%-- <p>
                                                        <span>
                                                            <asp:Label ID="lblVideoName" runat="server" Text="VideoName"></asp:Label></span>
                                                    </p>--%>
                                                    <video id="videoPlay" width="125" poster="<%# Eval("ThumbNailURL") %>" height="125" controls="" src="<%# Eval("VideoURL") %>" draggable="true">
                                                    </video>
                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:DataList>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Service-->
                <!--twitter-feed-end-->
                <footer class="footer_section" id="contact">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            <div class="footer_bottom"><span>© 2016 Athletics training. All Rights Reserved.</span> </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </form>
</body>
</html>
