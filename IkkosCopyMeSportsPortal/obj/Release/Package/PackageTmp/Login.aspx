﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="IkkosCopyMeSportsPortal.Login" EnableEventValidation="false" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <style type="text/css">
        .label {
            display: inline;
        }

        .textbox {
            display: inline;
        }
    </style>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1" />
    <title>Athletics App</title>
    <link rel="icon" href="img/favicon.png" type="image/png" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/linecons.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,900,700,700italic,400italic,300italic,300,100italic,100,900italic' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,500,700,800,600,300,200' rel='stylesheet' type='text/css' />
    <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->
    <script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/classie.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
    <script type="text/javascript">jQuery(document).ready(function ($) { tab = $('.tabs h3 a'); tab.on('click', function (event) { event.preventDefault(); tab.removeClass('active'); $(this).addClass('active'); tab_content = $(this).attr('href'); $('div[id$="tab-content"]').removeClass('active'); $(tab_content).addClass('active'); }); });</script>
    <!--[if lt IE 9]> <script src="js/respond-1.1.0.min.js"></script> <script src="js/html5shiv.js"></script> <script src="js/html5element.js"></script><![endif]-->
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="athletics-page">
                <!--Header_section-->
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="Login.aspx">
                                <img src="img/logo.png" /></a>
                        </div>
                        <%--  <ul class="nav navbar-nav">
                            <li><a href="Index.aspx">Home</a></li>
                            <li><a href="PackagesCreation.aspx">Package Creation</a></li>
                            <li><a href="UploadVideo.aspx">Upload video</a></li>
                            <li><a href="VideosList.aspx">Video list</a></li>
                            <li class="active"><a href="Login.aspx">Login</a></li>
                        </ul>--%>
                    </div>
                </nav>
                <!--Header_section-->
                <!--Top_content-->
                <section id="top_content" class="inner_banner">
                    <div class="inner_banner_cont">
                        <div class="gradient"></div>
                        <div class="container">
                            <div class="inner_banner_ttl">
                                <div class="inner_banner_left_ttl delay-03s animated wow fadeInLeft">
                                    <h2>LOGIN</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Top_content-->
                <!--Service-->
                <section id="service">
                    <div class="service_area">
                        <div class="row">
                            <div class="col-lg-12 col-xs-12">
                                <div class="form-wrap">
                                    <div class="tabs">
                                           <h3 class="login-tab"><a class="active" href="#login-tab-content">Login</a></h3>
                                     <%--   <h3 class="login-tab"><a href="#login-tab-content">Login</a></h3>--%>
                                    </div>
                                    <div class="tabs-content">
                                        <%--<div id="signup-tab-content" class="active">
                                            <form class="signup-form" action="" method="post">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="ddlSport" runat="server" Height="26px" Style="margin-left: 0px" Width="153px" AppendDataBoundItems="true">
                                                        <asp:ListItem Value="SelectSport">-- Select Sport-- </asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="sport" ControlToValidate="ddlSport" ErrorMessage="Please Select A Sport" runat="server" InitialValue="Select Sport"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="form-group">
                                                  
                                                    <asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" CssClass="textbox" Width="80%"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvUser" ErrorMessage="Please enter FirstName" ControlToValidate="txtFirstName" runat="server" />
                                                    <br />
                                                    <asp:TextBox ID="txtLastName" runat="server" placeholder="Last Name" CssClass="textbox" Width="80%"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please enter LastName" ControlToValidate="txtLastName" runat="server" />
                                                    
                                                    <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" CssClass="textbox" Width="80%" TextMode="Email"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please enter Email" ControlToValidate="txtEmail" runat="server" />
                                                    <br />
                                                    <asp:TextBox ID="txtPwd" runat="server" placeholder="Password" CssClass="textbox" Width="80%" TextMode="Password"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please enter Password" ControlToValidate="txtPwd" runat="server" />
                                                    <br />
                                                    <asp:TextBox ID="txtCnfrmPwd" runat="server" placeholder="Confirm Password" CssClass="textbox" Width="80%" TextMode="Password"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please enter Confirm Password" ControlToValidate="txtCnfrmPwd" runat="server" />
                                                    

                                                    <asp:TextBox ID="txtChannel" runat="server" placeholder="Channel" CssClass="textbox" Width="80%"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please enter ChannelName" ControlToValidate="txtChannel" runat="server" />
                                           
                                                </div>
                                                <div class="form-group">
                                                   
                                                    <asp:Button ID="btnSignin" runat="server" Text="Submit" class="btn btn-danger pull-right" OnClick="btnSignin_Click" />
                                                    <asp:Label ID="Error" runat="server"></asp:Label>
                                                </div>
                                            </form>
                                            <div class="col-lg-12 col-xs-12">
                                                <div class="help-text">
                                                    <p>By signing up, you agree to our</p>
                                                    <p><a href="#">Terms of service</a></p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>--%>
                                        <div id="login-tab-content" class="active">
                                            <form class="login-form" action="" method="post">
                                                <div class="form-group">
                                                    <%-- <input type="text" class="form-control" placeholder="Email or Username" required />--%>
                                                    <asp:TextBox ID="txtUserName" runat="server" placeholder="Email or UserName" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="form-group">
                                                    <%--<input type="password" class="input" id="user_pass" autocomplete="off" placeholder="Password" required />--%>
                                                    <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                                </div>
                                           <%--     <div class="form-group">
                                                    <input type="checkbox" class="checkbox" id="remember_me" /><label for="remember_me">Remember Me</label>
                                                </div>--%>
                                                <div class="form-group">
                                                    <%-- <input type="submit" class="button" value="Login" />--%>
                                                    <asp:Button ID="btnLogin" runat="server" Text="Login"  class="btn btn-danger"  OnClick="btnLogin_Click1" />
                                                    <asp:Label ID="lblLogin" runat="server"></asp:Label>
                                                </div>
                                            </form> 
                                            <div class="help-text">
                                                <p><a href="#">Forget your password?</a></p>
                                            </div>
                                             <div class="help-text">
                                                <p>New User ? <a href="SignUp.aspx">SIGN UP</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Service-->
                <!--twitter-feed-end-->
                <footer class="footer_section" id="contact">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            <div class="footer_bottom"><span>© 2016 Athletics training. All Rights Reserved.</span> </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </form>
</body>
</html>
