﻿using System;
using System.Collections.Generic;

namespace IkkosCopyMeSportsPortal
{
    public class DTO
    {
    }
    public partial class GetSportsDTO
    {
        public GetSportsDTO()
        {

        }

        public int SportId { get; set; }
        public string SportName { get; set; }
        public string SportsImageUrl { get; set; }
        public string SportsBannerImageUrl { get; set; }
    }
    public partial class AdminUsersDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public int SportId { get; set; }
        public string ChannelName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ProfilePicURL { get; set; }
        public string VideoURL { get; set; }
        public string ThumbNailURL { get; set; }
        public int Duration { get; set; }
        public string Description { get; set; }
        public int PricingCatalogId { get; set; }
    }
    public partial class LoginDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }

    }

    public partial class VideoInfoDTO
    {
        public int Id { get; set; }
        public string VideoURL { get; set; }
        public string ActorName { get; set; }
        public string MomentName { get; set; }
        public string IntroVideoURL { get; set; }
        public int Status { get; set; }
        public int CreatedBy { get; set; }
        public int Duration { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ThumbNailURL { get; set; }
    }
    public partial class PricingCatalogDTO
    {
        public int Id { get; set; }
        public string Price { get; set; }
        public string SKUID { get; set; }
        public DateTime CreatedDate { get; set; }
        public int PricingType { get; set; }
    }
    public partial class PackagesInfoDTO
    {
        public int PackageId { get; set; }
        public string PackageName { get; set; }
        public string Description { get; set; }
        public int LibraryId { get; set; }
        public string Format { get; set; }
        public string VideoURL { get; set; }
        public string ThumbNailURL { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public int PricingCatalogId { get; set; }
        public string Price { get; set; }
        public string SkuId { get; set; }
        public int Rating { get; set; }
        public int IsFavourite { get; set; }
        public int IsPayment { get; set; }
        public List<PackageVideoInfo> PackageVideoDetails { get; set; }
        public List<PackageVideoURLInfo> PackageVideoUrlDetails { get; set; }
    }
    public partial class PackageVideoInfo
    {
        public int ActualVideoId { get; set; }
        public int PreIntroVideoId { get; set; }
        public int PostIntroVideoId { get; set; }
    }
    public class PackageVideoURLInfo
    {
        public string ActualVideoUrl { get; set; }
        public string PreIntroVideoUrl { get; set; }
        public string PostIntroVideoUrl { get; set; }
    }
}