﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test3.aspx.cs" Inherits="IkkosCopyMeSportsPortal.test3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.8.24/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://code.jquery.com/ui/1.8.24/themes/blitzer/jquery-ui.css" rel="stylesheet"
        type="text/css" />
    


    <title></title>
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }

        img {
            height: 100px;
            width: 100px;
            margin: 2px;
        }

        .draggable {
            filter: alpha(opacity=60);
            opacity: 0.6;
        }

        .dropped {
            position: static !important;
        }

        #dvSource, #dvDest {
            border: 5px solid #ccc;
            padding: 5px;
            min-height: 100px;
            width: 430px;
        }
    </style>
    <script>
        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
        }

        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
           ev.target.appendChild(document.getElementById(data));
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:DataList ID="dlProducts" runat="server">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
                <div id="dvSource">
                    <%-- <video id="videoPlay" poster="/images/videoposter.jpg"
                        controls="controls" width="168" height="168">
                        <source src="<%# Eval("VideoURL") %>" type="video/mp4" draggable="true" />
                    </video>--%>
                    <p>
                        <span>
                            <asp:Label ID="lblVideoName" runat="server" Text="VideoName"></asp:Label></span>
                    </p>
                    <video id="videoPlay" width="320" poster="/images/videoposter.jpg" height="240" controls="" src="<%# Eval("VideoURL") %>">
                    </video>
                </div>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:DataList>
        <asp:Label ID="test" runat="server"></asp:Label>
        <%--  <div id="dvSource">
            <img alt="" src="images/Chrysanthemum.jpg" />
            <img alt="" src="images/Desert.jpg" />
            <img alt="" src="images/Hydrangeas.jpg" />
            <img alt="" src="images/Jellyfish.jpg" />
            <img alt="" src="images/Koala.jpg" />
            <img alt="" src="images/Lighthouse.jpg" />
            <img alt="" src="images/Penguins.jpg" />
            <img alt="" src="images/Tulips.jpg" />
        </div>--%>
        <hr />
        <div id="dvDest">
            Drop here
           
        </div>

        <script type="text/javascript">
            $(function () {
                $("#dvSource video").draggable({
                    revert: "invalid",
                    refreshPositions: true,
                    drag: function (event, ui) {
                        ui.helper.addClass("draggable");
                        console.log("drag called added draggable");
                    },
                    stop: function (event, ui) {
                        ui.helper.removeClass("draggable");
                        console.log("stop called removed draggable");
                        // var image = this.src.split("/")[this.src.split("/").length - 1];
                        // var image = this;//.split("/")[this.src.split("/").length - 1];

                        if ($.ui.ddmanager.drop(ui.helper.data("draggable"), event)) {
                           
                            var dlId = document.getElementById("lblVideoName");
                            console.log("about to get videoplay");

                            var x = document.getElementById("videoPlay").src;
                            alert(x);
                            // $("#test").append(x);
                          //  $("#test").html(x);
                           alert(x + " dropped.");

                        }
                        else {
                            //alert(image + " not dropped.");
                        }
                    }
                });
                $("#dvDest").droppable({
                    drop: function (event, ui) {

                        if ($("#dvDest video").length == 0) {
                            $("#dvDest").html("");
                        }

                        ui.draggable.addClass("dropped");
                        $("#dvDest").append(ui.draggable);
                    }
                });
            });
    </script>

    </form>
</body>
</html>
