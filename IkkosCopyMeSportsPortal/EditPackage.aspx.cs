﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using IkkosMultiSportsCommon;
using Newtonsoft.Json.Linq;

namespace IkkosCopyMeSportsPortal
{
    public partial class EditPackage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null)
            {
                Response.Redirect("Login.aspx", false);
            }
            string PackageId = Request.QueryString["PackageId"];
            CustomResponse Result1 = APICalls.Get("Package/GetPackagesByPackageId?packageId=" + PackageId);
            if (Result1.Response != null)
            {
                PackagesInfoDTO pack = new PackagesInfoDTO();

                JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                serializer2.MaxJsonLength = 1000000000;
                var Package = Result1.Response.ToString();
                JObject PackageData = JObject.Parse(Package);

                var packagesInfoDTO = serializer2.Deserialize<PackagesInfoDTO>(Package);

                int userid = packagesInfoDTO.PackageId;
                string Packagename = packagesInfoDTO.PackageName;
                string PackageDesc = packagesInfoDTO.Description;
                int PricingId = packagesInfoDTO.PricingCatalogId;  

                int packCount = packagesInfoDTO.PackageVideoUrlDetails.Count; 


           
                DataView PricingView = PackagePricing();

                ddlPricing.DataSource = PricingView;
                ddlPricing.DataValueField = "Id";
                ddlPricing.DataTextField = "Price";
                ddlPricing.DataBind();

                txtPackageName.Text = Packagename;
                txtDescription.Text = PackageDesc;             
                ddlPricing.SelectedValue = PricingId.ToString();

            }
            else
            {

            }
            int LogUserId = (int)System.Web.HttpContext.Current.Session["UserId"];
            CustomResponse Res = APICalls.Get("Videos/GetAdminVideos?adminUserId=" + LogUserId);
            if (Result1.Response != null)
            {
                JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                serializer2.MaxJsonLength = 1000000000;
                var Videos = Res.Response.ToString();

                List<VideoInfoDTO> VideosList = serializer2.Deserialize<List<VideoInfoDTO>>(Videos);
                //var VideosJson = new JavaScriptSerializer().Serialize(VideosList);
                //var replVideosJson =  VideosJson.Replace("\u0026", "&");

                //DataTable UserVideosTable = JsonStringToDataTable(replVideosJson);
                ////SportsTable.Rows.Add("Select Sport", "Select Sport");
                //DataView view = UserVideosTable.DefaultView;
                dtVideos.DataSource = VideosList;
                dtVideos.RepeatColumns = 2;
                dtVideos.RepeatDirection = RepeatDirection.Horizontal;
                dtVideos.DataBind();
            }
            else
            {

            }
        }

        private DataView PackagePricing()
        {

            CustomResponse result = APICalls.Get("Package/GetPricingCatalog");
            JavaScriptSerializer serializer2 = new JavaScriptSerializer();
            serializer2.MaxJsonLength = 1000000000;
            var sports = result.Response.ToString();

            List<PricingCatalogDTO> PricingData = serializer2.Deserialize<List<PricingCatalogDTO>>(sports);
            var PricingJson = new JavaScriptSerializer().Serialize(PricingData);

            DataTable PackagePricingTable = JsonStringToDataTable(PricingJson);
            //SportsTable.Rows.Add("Select Sport", "Select Sport");
            //DataView view = PackagePricingTable.DefaultView;
            //view.Sort = "Id";
            //ddlPricing.DataSource = view;
            //ddlPricing.DataValueField = "Id";
            //ddlPricing.DataTextField = "Price";
            //ddlPricing.DataBind();
            DataRow[] dr = PackagePricingTable.Select("PricingType=2");
            DataTable newTable = new DataTable();
            newTable = PackagePricingTable.Clone();
            foreach (DataRow row in dr)
            {
                newTable.ImportRow(row);
            }

            DataView view = newTable.DefaultView;
            view.Sort = "Id";
            return view;
            //ddl.DataSource = view;
            //ddlPricing.DataValueField = "Id";
            //ddlPricing.DataTextField = "Price";
            //ddlPricing.DataBind();


        }
        public DataTable JsonStringToDataTable(string jsonString)
        {
            DataTable dt = new DataTable();
            string[] jsonStringArray = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{");
            List<string> ColumnsName = new List<string>();
            foreach (string jSA in jsonStringArray)
            {
                string[] jsonStringData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                foreach (string ColumnsNameData in jsonStringData)
                {
                    try
                    {
                        int idx = ColumnsNameData.IndexOf(":");
                        string ColumnsNameString = ColumnsNameData.Substring(0, idx - 1).Replace("\"", "");
                        if (!ColumnsName.Contains(ColumnsNameString))
                        {
                            ColumnsName.Add(ColumnsNameString);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Error Parsing Column Name : {0}", ColumnsNameData));
                    }
                }
                break;
            }
            foreach (string AddColumnName in ColumnsName)
            {
                dt.Columns.Add(AddColumnName);
            }
            foreach (string jSA in jsonStringArray)
            {
                string[] RowData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                DataRow nr = dt.NewRow();
                foreach (string rowData in RowData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        string RowColumns = rowData.Substring(0, idx - 1).Replace("\"", "");
                        string RowDataString = rowData.Substring(idx + 1).Replace("\"", "");
                        nr[RowColumns] = RowDataString;
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
                dt.Rows.Add(nr);
            }
            return dt;
        }
        public static string GetJsonString(object data)
        {
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            return javaScriptSerializer.Serialize(data);

        }
    }
}