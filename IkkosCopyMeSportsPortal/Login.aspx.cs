﻿using System;
using IkkosMultiSportsCommon;
using Newtonsoft.Json.Linq;


namespace IkkosCopyMeSportsPortal
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Abandon();
                Session.Clear();

            }
        }

        protected void btnLogin_Click1(object sender, EventArgs e)
        {
            string UserName = txtUserName.Text;
            string Password = txtPassword.Text;
            if (UserName != null && Password != null)
            {

                LoginDTO LoginUser = new LoginDTO();
                LoginUser.Email = UserName;
                LoginUser.Password = Password;
                CustomResponse res = APICalls.Get("Users/UserLogin?emailaddress=" + UserName + "&password=" + Password);
                string status = res.Status.ToString();
                string status1 = "Successful";
                if (status == status1)
                {
                    string Result = res.Response.ToString();
                    JObject o = JObject.Parse(Result);
                    int userid = (int)o["Id"];

                    Session["UserId"] = userid;
                    Response.Redirect("Index.aspx");
                }
                else
                {
                    lblLogin.Text = res.Message;
                    lblLogin.ForeColor = System.Drawing.Color.Red;
                }


            }
            else
            {
                lblLogin.Text = "Please Enter Email And Password";
                lblLogin.ForeColor = System.Drawing.Color.Red;
            }
        }


    }
}