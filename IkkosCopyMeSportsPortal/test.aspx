﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="IkkosCopyMeSportsPortal.test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="FlowPlayer/flowplayer-3.2.12.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        flowplayer("a.player", "FlowPlayer/flowplayer-3.2.16.swf", {
            plugins: {
                pseudo: { url: "FlowPlayer/flowplayer.pseudostreaming-3.2.12.swf" }
            },
            clip: { provider: 'pseudo', autoPlay: false },
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DataList ID="DataList1" runat="server" BackColor="Gray" BorderColor="#666666"
                BorderStyle="None" BorderWidth="2px" CellPadding="3" CellSpacing="2"
                Font-Names="Verdana" Font-Size="Small" GridLines="Both" RepeatColumns="3" RepeatDirection="Horizontal"
                Width="600px">
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" Font-Size="Large" ForeColor="White"
                    HorizontalAlign="Center" VerticalAlign="Middle" />
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemStyle BackColor="White" ForeColor="Black" BorderWidth="2px" />
                <ItemTemplate>
                    <asp:Image ID="imgEmp" runat="server" Width="100px" Height="120px" ImageUrl='<%# Bind("PhotoPath", "~/photo/{0}") %>' Style="padding-left: 40px" />
                    <a class="player" style="height: 300px; width: 300px; display: block" href='<%# Eval("Id", "FileCS.ashx?Id={0}") %>'>
                        <br />
                        <b>Employee Name:</b>
                        <asp:Label ID="lblCName" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                        <br />
                        <b>Designation:</b>
                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                        <br />
                        <b>City:</b>
                        <asp:Label ID="lblCity" runat="server" Text=' <%# Bind("City") %>'></asp:Label>
                        <br />
                        <b>Country:</b>
                        <asp:Label ID="lblCountry" runat="server" Text='<%# Bind("Country") %>'></asp:Label>
                        <br />
                </ItemTemplate>
            </asp:DataList>

        </div>
    </form>
</body>
</html>
