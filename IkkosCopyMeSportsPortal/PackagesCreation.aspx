﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PackagesCreation.aspx.cs" Inherits="IkkosCopyMeSportsPortal.PackagesCreation" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1" />
    <title>Athletics App</title>
    <link rel="icon" href="img/favicon.png" type="image/png" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/linecons.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,900,700,700italic,400italic,300italic,300,100italic,100,900italic' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,500,700,800,600,300,200' rel='stylesheet' type='text/css' />
    <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->
    <script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/classie.js"></script>
    <!--[if lt IE 9]> <script src="js/respond-1.1.0.min.js"></script> <script src="js/html5shiv.js"></script> <script src="js/html5element.js"></script><![endif]-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="Scripts/ScrollableGridPlugin.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.8.24/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://code.jquery.com/ui/1.8.24/themes/blitzer/jquery-ui.css" rel="stylesheet"
        type="text/css" />

    

    <style type="text/css">
        

        /*img {
            height: 100px;
            width: 100px;
            margin: 2px;
        }
 #txtTraingingVideos {
            border-radius: 25px;
            border: 2px solid #CCCCCC;
            padding: 20px;
            width: 140px;
            height: 150px;
        }

        #txtInstVideos {
            border-radius: 25px;
            border: 2px solid #CCCCCC;
            padding: 20px;
            width: 140px;
            height: 150px;
        }



        */
        video { padding:0 !important; margin:0 !important;
        }
        .draggable {
            filter: alpha(opacity=60);
            opacity: 0.6;
        }

        .dropped {
            position: static !important;
        }

        #dvSource {
            border: 4px solid #ccc;
            padding: 5px; margin:0px 20px 20px 0;
            min-height: 210px;
            max-width: 190px; width:100%; overflow:hidden;
        }
        #dvSource p{
            font-size:13px !important; line-height:16px !important; padding:0; margin:0;
        }

        #dvDest, #dvDest1, #dvDest2, #dvDest3, #dvDest4, #dvDest5 {
            border: 3px solid #ccc;
            padding: 5px;
            min-height: 145px;
            width: 140px;
        }

        #dvDest6, #dvDest7, #dvDest8, #dvDest9, #dvDest10, #dvDest11 {
            border: 3px solid #ccc;
            padding: 5px;
            min-height: 145px;
            width: 140px;
        }

        #dvDest12, #dvDest13, #dvDest14 {
            border: 3px solid #ccc;
            padding: 5px;
            min-height: 145px;
            width: 140px;
        }
    </style>


    <script>
        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
        }

        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
        }
    </script>

    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                helper: "clone",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                   // $(this).clone($(ui.draggable).clone());
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));                            

                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));

                    $("#<%=lbldvDest0.ClientID %>").val(ui.draggable.attr('videoId'));

                    // ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest video").length == 0) {
                        $("#dvDest").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest").append(ui.draggable);
                    //draggable.clone().appendTo(droppable);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest1").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest1.ClientID %>").val(ui.draggable.attr('videoId'));


                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest1 video").length == 0) {
                        $("#dvDest1").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest1").append(ui.draggable);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest2").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest2.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest2 video").length == 0) {
                        $("#dvDest2").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest2").append(ui.draggable);
                }
            });
        });
    </script>


    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest3").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest3.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest3 video").length == 0) {
                        $("#dvDest3").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest3").append(ui.draggable);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest4").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest4.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest4 video").length == 0) {
                        $("#dvDest4").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest4").append(ui.draggable);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest5").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest5.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest5 video").length == 0) {
                        $("#dvDest5").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest5").append(ui.draggable);
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest6").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest6.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest6 video").length == 0) {
                        $("#dvDest6").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest6").append(ui.draggable);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest7").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest7.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest7 video").length == 0) {
                        $("#dvDest7").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest7").append(ui.draggable);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest8").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest8.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest8 video").length == 0) {
                        $("#dvDest8").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest8").append(ui.draggable);
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest9").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest9.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest9 video").length == 0) {
                        $("#dvDest9").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest9").append(ui.draggable);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest10").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest10.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest10 video").length == 0) {
                        $("#dvDest10").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest10").append(ui.draggable);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest11").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest11.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest11 video").length == 0) {
                        $("#dvDest11").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest11").append(ui.draggable);
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest12").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest12.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest12 video").length == 0) {
                        $("#dvDest12").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest12").append(ui.draggable);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest13").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest13.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest13 video").length == 0) {
                        $("#dvDest13").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest13").append(ui.draggable);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#dvSource video").draggable({
                revert: "invalid",
                refreshPositions: true,
                drag: function (event, ui) {
                    ui.helper.addClass("draggable");
                    console.log("drag called added draggable");
                },
                stop: function (event, ui) {
                    ui.helper.removeClass("draggable");
                    console.log("stop called removed draggable");
                }
            });
            $("#dvDest14").droppable({
                drop: function (event, ui) {
                    console.log("dvDest drop called");
                    console.log("video source=" + ui.draggable.attr('src'));
                    var draggedVideoUrl = ui.draggable.attr('src')
                    var draggedVideoId = ui.draggable.attr('videoId');

                    //var videoInfoDto = {
                    //    "PreInstructionVideoId": draggedVideoId
                    //    ,


                    //}

                    console.log("custom video Id=" + ui.draggable.attr('videoId'));
                    $("#<%=lbldvDest14.ClientID %>").val(ui.draggable.attr('videoId'));

                    ui.draggable.draggable("option", "revert", false);
                    if ($("#dvDest14 video").length == 0) {
                        $("#dvDest14").html("");
                    }
                    ui.draggable.addClass("dropped");
                    $(this).droppable("destroy");
                    $("#dvDest14").append(ui.draggable);
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            var click = 1;
            $("#btnAdd").click(function () {
                if (click == 1) {
                    var TVideo1 = $('#lbldvDest1').val();
                    if (TVideo1 != "") {
                        click = click + 1;
                        $("#ROW2").show();
                    }
                    else {
                        alert("Training Video Is Mandatory!!!");
                    }
                }
                else if (click == 2) {
                    var TVideo2 = $('#lbldvDest4').val();
                    if (TVideo2 != "") {
                        click = click + 1;
                        $("#ROW3").show();
                    }
                    else {
                        alert("Training Video Is Mandatory!!!");
                    }
                }
                else if (click == 3) {
                    var TVideo3 = $('#lbldvDest7').val();
                    if (TVideo3 != "") {
                        click = click + 1;
                        $("#ROW4").show();
                    }
                    else {
                        alert("Training Video Is Mandatory!!!");
                    }
                }
                else if (click == 4) {
                    var TVideo4 = $('#lbldvDest10').val();
                    if (TVideo4 != "") {
                        click = click + 1;
                        $("#ROW5").show();
                    }
                    else {
                        alert("Training Video Is Mandatory!!!");
                    }

                }
            });

        });
        $(document).ready(function () {

            $('#up1').change(function () {
                var uploadpath = $('#up1').val();
                var fileExtension = uploadpath.substring(uploadpath.lastIndexOf(".") + 1, uploadpath.length);
                if (fileExtension == "mp4") {
                }
                else {
                    $('#up1').val('');
                    alert("Only MP4 Format Allowed!!!");
                    return false;
                }
            });
        });
    </script>
</head>
<body>
    <form id="Form1" runat="server">
        <div class="container">
            <div class="athletics-page">
                <!--Header_section-->
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand">
                                <img src="img/logo.png" /></a>
                        </div>
                        <ul class="nav navbar-nav pull-right">
                            <li><a href="Index.aspx">Home</a></li>
                            <li class="active"><a href="PackagesCreation.aspx">Package Creation</a></li>
                            <%-- <li><a href="UploadVideo.aspx">Upload video</a></li>--%>
                            <li><a href="VideosList.aspx">Videos</a></li>
                            <li><a href="PackagesList.aspx">Packages List</a></li>
                            <li><a href="Login.aspx">Log Out</a></li>
                            <%--  <li><a href="Login.aspx">Login</a></li>--%>
                        </ul>
                    </div>
                </nav>
                <!--Header_section-->
                <!--Top_content-->
                <section id="top_content" class="inner_banner">
                    <div class="inner_banner_cont">
                        <div class="gradient"></div>
                        <div class="container">
                            <div class="inner_banner_ttl">
                                <div class="inner_banner_left_ttl delay-03s animated wow fadeInLeft">
                                    <h2>Packages <strong>Creation</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Top_content-->
                <!--Service-->
                <section id="service">
                    <div class="service_area">
                        <div class="row">

                            <div class="col-lg-6 col-xs-6">
                                <div class="formcontent text-center to-animate">
                                    <form class="contact-form">
                                        <h2>PACKAGES CREATION</h2>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtPackageName" runat="server" placeholder="Package Name" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvUser" ErrorMessage="Please enter PackageName" ControlToValidate="txtPackageName" runat="server" ForeColor="Red" />
                                            <%-- <input type="name" class="form-control" placeholder="Package Name">--%>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtDescription" runat="server" placeholder="Description" CssClass="form-control" Rows="7" TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please enter PackageName" ControlToValidate="txtDescription" runat="server" ForeColor="Red" />
                                            <%--<textarea class="form-control" id="message" rows="7" placeholder="Description"></textarea>--%>
                                        </div>
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddlPricing" runat="server" Height="30px" Width="430px" CssClass="textbox" AppendDataBoundItems="true">
                                                <asp:ListItem Value="SelectSport">-- Select Package Pricing-- </asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblIntro" runat="server" Font-Size="13"
                                                Text="Introduction Video" Width="325"></asp:Label>
                                            <%--<input type="email" class="form-control" placeholder="Intro Video">--%>
                                            <asp:FileUpload ID="up1" runat="server" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ErrorMessage="Introduction Video Required" ControlToValidate="up1"
                                                runat="server" Display="Dynamic" ForeColor="Red" />
                                        </div>
                                        <div class="form-group">
                                            <p>
                                                <span style="float: left; font-size: 10pt">PreInstruction Videos:</span>
                                                <span style="float: initial; font-size: 11pt">Training Video:</span>
                                                <span style="float: right; font-size: 10pt">Post Instruction Video:</span>
                                            </p>
                                            <br />
                                            <table style="width: 100%;">
                                                <tr id="ROW1" runat="server">
                                                    <td style="width: 33%;">
                                                        <div id="dvDest" runat="server">
                                                            <p style="font-size: 100%;">PreInstruction Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest0" runat="server" />
                                                    </td>
                                                    <td style="width: 33%;">
                                                        <div id="dvDest1">
                                                            <p style="font-size: 100%;">Training Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest1" runat="server" />
                                                    </td>
                                                    <td style="width: 33%;">
                                                        <div id="dvDest2">
                                                            <p style="font-size: 100%;">Post Instruction Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest2" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr id="ROW2" runat="server" style="display: none;">
                                                    <td style="width: 33%;">
                                                        <div id="dvDest3">
                                                            <p style="font-size: 100%;">PreInstruction Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest3" runat="server" />
                                                    </td>
                                                    <td style="width: 33%;">
                                                        <div id="dvDest4">
                                                            <p style="font-size: 100%;">Training Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest4" runat="server" />
                                                    </td>
                                                    <td style="width: 33%;">
                                                        <div id="dvDest5">
                                                            <p style="font-size: 100%;">Post Instruction Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest5" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr id="ROW3" runat="server" style="display: none;">
                                                    <td>
                                                        <div id="dvDest6">
                                                            <p style="font-size: 100%;">PreInstruction Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest6" runat="server" />
                                                    </td>
                                                    <td>
                                                        <div id="dvDest7">
                                                            <p style="font-size: 100%;">Training Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest7" runat="server" />
                                                    </td>
                                                    <td>
                                                        <div id="dvDest8">
                                                            <p style="font-size: 100%;">Post Instruction Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest8" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr id="ROW4" runat="server" style="display: none;">
                                                    <td>
                                                        <div id="dvDest9">
                                                            <p style="font-size: 100%;">PreInstruction Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest9" runat="server" />
                                                    </td>
                                                    <td>
                                                        <div id="dvDest10">
                                                            <p style="font-size: 100%;">Training Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest10" runat="server" />
                                                    </td>
                                                    <td>
                                                        <div id="dvDest11">
                                                            <p style="font-size: 100%;">Post Instruction Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest11" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr id="ROW5" runat="server" style="display: none;">
                                                    <td>
                                                        <div id="dvDest12">
                                                            <p style="font-size: 100%;">PreInstruction Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest12" runat="server" />
                                                    </td>
                                                    <td>
                                                        <div id="dvDest13">
                                                            <p style="font-size: 100%;">Training Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest13" runat="server" />
                                                    </td>
                                                    <td>
                                                        <div id="dvDest14">
                                                            <p style="font-size: 100%;">Post Instruction Video:</p>
                                                            <p style="font-size: 100%;">Drop here:</p>
                                                        </div>
                                                        <asp:HiddenField ID="lbldvDest14" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>

                                                    <td style="text-align: center;">
                                                        <%-- <a href="#">
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                            <input type="button" value="ADD" id="btnAdd" class="btn btn-primary" />
                                                        </a>--%>
                                                        <button type="button" class="btn btn-default" aria-label="my button" style="border: 0px" id="btnAdd">
                                                            <span class="glyphicon glyphicon-plus-sign" style="color: #a0a0a0; font-size: 20px; vertical-align: middle;" aria-hidden="true"></span>
                                                            ADD
                                                        </button>
                                                        <%--<asp:ImageButton ID="imgBtnAdd" ImageUrl="~/img/favicon.ico" runat="server" />--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div>
                                            <asp:Button ID="Button1" runat="server" Text="Create Package" CssClass="btn btn-danger" OnClick="btnCreate_Click" />
                                            <asp:Label ID="lblError" runat="server"></asp:Label>
                                            <asp:Label ID="lbldest1" runat="server"></asp:Label>
                                        </div>
                                        <div>
                                            <asp:Label ID="Error" runat="server"></asp:Label>
                                            <asp:Label ID="lblVideoId" runat="server" Visible="false"></asp:Label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-6">
                                <div class="formcontent text-center to-animate">
                                    <form class="contact-form">
                                        <h2>VIDEOS LIST</h2>
                                        <div style="height: 750px; width: 430px; overflow: Auto; overflow-x: hidden;">
                                            <asp:DataList ID="dtVideos" runat="server">
                                                <HeaderTemplate>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <th>Videos
                                                            </th>

                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div id="dvSource">
                                                        <%-- <video id="videoPlay" poster="/images/videoposter.jpg"
                        controls="controls" width="168" height="168">
                        <source src="<%# Eval("VideoURL") %>" type="video/mp4" draggable="true" />
                    </video>--%>
                                                        <%-- <p>
                                                        <span>
                                                            <asp:Label ID="lblVideoName" runat="server" Text="VideoName"></asp:Label></span>
                                                    </p>--%>
                                                        <video id="videoPlay" width="120" poster="<%# Eval("ThumbNailURL") %>" height="120" controls=""
                                                            src="<%# Eval("VideoURL") %>" draggable="true" videoid="<%# Eval("Id") %>">
                                                        </video>
                                                       <div class="col-lg-12 col-sm-12">
                                                       <p><asp:Label ID="lbl" runat="server" Text='<%#Eval("Id")%>' Visible="false"></asp:Label></p> 
                                                        
                                                       <p><b>Video Name:</b></p> 
                                                        <p><asp:Label ID="lblCName" runat="server" Text='<%# Bind("MomentName") %>'></asp:Label></p>

                                                        </div>
                                                    </div>
                                                </ItemTemplate>

                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:DataList>
                                        </div>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Service-->
                <!--twitter-feed-end-->
                <footer class="footer_section" id="contact">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            <div class="footer_bottom"><span>© 2016 Athletics training. All Rights Reserved.</span> </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="js/index.js"></script>

    </form>
</body>
</html>
